## About

#### Overview
[iMSTK](https://www.imstk.org/) is a C++ based free & open-source toolkit that aids rapid prototyping of real-time multi-modal surgical simulation scenarios. [Unity](https://unity.com/) is a multi-platform game engine designed to create 2D and 3D games.

This repository contains imstk's Unity plugin, C# scripts, demo scenarios and resources needed to build imstk Unity package. 

#### License
[Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

## Resources

#### Documentation

User documentation: [https://imstk-unity.readthedocs.io/en/latest/](https://imstk-unity.readthedocs.io/en/latest/)

API documentation: https://imstk-unity.gitlab.io/documentation/

#### Issue-tracker
https://gitlab.kitware.com/iMSTK/imstk-unity/issues


## Building iMSTK-Unity

#### Prerequisites
* Git
* CMake 3.15.x
* Unity 2019.3.2f1
* Visual Studio 2019 (Not tested with older versions)
* iMSTK (Tag: [imstk-unity](https://gitlab.kitware.com/iMSTK/iMSTK/-/tags/imstk-unity))


#### Configuration and Build

1. Clone the Imstk repository from the url: https://gitlab.kitware.com/iMSTK/iMSTK.git

2. If using an existing Imstk repo you may need to force update your Imstk repo tags with "git fetch --tags -f"

3. Checkout the imstk-unity tag into a new brach: git checkout tags/imstk-unity -b <branch name>

4. Build Imstk (refer to instructions on the Imstk repo page)

5. Clone imstk-unity repository from the url into a separate folder: https://gitlab.kitware.com/iMSTK/imstk-unity.git

6. Run cmake with:

    * source directory = <your cloned repo>/iMSTKUnityWrapper
    * build directory = <somewhere outside of this repo>
    * iMSTK_DIR = <path to your iMSTK build directory>/install/lib/cmake/iMSTK-3.0
    * Optionally one may change the CMAKE_INSTALL_PREFIX to install to a different Unity project. It defaults to the adjacent demo project provided in this repo.

7. Then build the install target of the resulting project. This will copy your built library as well as the dependencies to the install directory.

After your project is up and running you can make modifications and build install.

#### Debugging

To debug in visual studios:

* Build and install Imstk debug configuration
* Build and install Imstk-Unity debug configuration
* Toggle “Project Settings->Imstk Settings->Use Debug” to true in the Unity Editor to inform it to use the debug dll
* In the iMSTKUnityWrapper project after selecting debug config click “Debug->Attach to Process” and select the Unity exe.

#### Notes

* Ensure usage of the imstk unity tag. This is the last known commit of Imstk to work with unity.
* When opening a fresh build the dlls often don't load before importers are run. Thus resources fail to load. If this happens, close the Unity Editor and reopen, then reimport the resources.