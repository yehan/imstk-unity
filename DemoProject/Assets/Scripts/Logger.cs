﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    public sealed class Logger
    {
        // Register its reverse pinvoke for string print callback
        public static void Register()
        {
            setSink(
                Marshal.GetFunctionPointerForDelegate(new Action<IntPtr>(PrintStringExt)));
        }

        // Reverse Pinvoked PrintString
        public static void PrintStringExt(IntPtr str) { Debug.Log(Marshal.PtrToStringAnsi(str)); }
        // Prints a string
        public static void PrintString(string str) { Debug.Log(str); }


        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void genLogger();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void deleteLogger();

        [DllImport(PInvoke.ImstkUnityLibName)]
        private static extern void setSink(IntPtr funcPtr);
    }
}