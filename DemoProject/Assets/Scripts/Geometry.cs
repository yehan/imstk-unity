﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    public enum GeometryType
    {
        Plane,
        Sphere,
        Cylinder,
        Cube,
        Capsule,
        PointSet,
        SurfaceMesh,
        TetrahedralMesh,
        HexahedralMesh,
        LineMesh,
        Decal,
        DecalPool,
        RenderParticles,
        ImageData
    };
    /// <summary>
    /// Struct to represent info of meshes of homogenous cell type
    /// </summary>
    public struct GeometryInfo
    {
        public int vertexCount;
        public int cellCount;
        public GeometryType type;
    }
    /// <summary>
    /// Struct to represent meshes of homogenous cell type
    /// </summary>
    public struct PrimitiveGeometry
    {
        public GeometryInfo info;
        public IntPtr vertexBufferPtr;
        public IntPtr indexBufferPtr;
    }

    /// <summary>
    /// This is the geometry class, for holding geometry Unity cannot, also serves as an
    /// interface to the c api geometry, this *may* eventually be subclassed
    /// </summary>
    public class Geometry : ScriptableObject
    {
        /// <summary>
        /// Map of some geometry types to number of cell points
        /// </summary>
        public static Dictionary<GeometryType, int> typeToNumPts = new Dictionary<GeometryType, int>()
        {
            { GeometryType.PointSet, 1 },
            { GeometryType.LineMesh, 2 },
            { GeometryType.SurfaceMesh, 3 },
            { GeometryType.TetrahedralMesh, 4 },
            { GeometryType.HexahedralMesh, 8 }
        };
        /// <summary>
        /// Map of GeometryType to MeshTopology
        /// </summary>
        public static Dictionary<GeometryType, MeshTopology> geomTypeToMeshTopology = new Dictionary<GeometryType, MeshTopology>()
        {
            { GeometryType.PointSet, MeshTopology.Points },
            { GeometryType.LineMesh, MeshTopology.Lines },
            { GeometryType.SurfaceMesh, MeshTopology.Triangles }
        };
        /// <summary>
        /// Map of MeshTopology to GeometryType
        /// </summary>
        public static Dictionary<MeshTopology, GeometryType> meshTopologyToGeomType = new Dictionary<MeshTopology, GeometryType>()
        {
            { MeshTopology.Points, GeometryType.PointSet },
            { MeshTopology.Lines, GeometryType.LineMesh },
            { MeshTopology.Triangles, GeometryType.SurfaceMesh }
        };

        public GeometryType type;
        public Vector3[] vertices = new Vector3[0];
        public Vector2[] texCoords = new Vector2[0];
        public int[] indices = new int[0];
        public GCHandle gcVertices;
        public GCHandle gcIndices;

        public void CopyTo(Geometry geometry)
        {
            geometry.type = type;
            geometry.vertices = new Vector3[vertices.Length];
            vertices.CopyTo(geometry.vertices, 0);
            geometry.texCoords = new Vector2[texCoords.Length];
            texCoords.CopyTo(geometry.texCoords, 0);
            geometry.indices = new int[indices.Length];
            indices.CopyTo(geometry.indices, 0);
        }

        public bool IsVolume { get { return type == GeometryType.TetrahedralMesh || type == GeometryType.HexahedralMesh; } }

        // Pins the vertices and indices
        public void Pin()
        {
            PinVertices();
            PinIndices();
        }
        public void PinVertices() { gcVertices = GCHandle.Alloc(vertices, GCHandleType.Pinned); }
        public void PinIndices() { gcIndices = GCHandle.Alloc(indices, GCHandleType.Pinned); }

        public void Free()
        {
            FreeVertices();
            FreeIndices();
        }
        public void FreeVertices() { gcVertices.Free(); }
        public void FreeIndices() { gcIndices.Free(); }

        public IntPtr GetVertexPtr() { return gcVertices.AddrOfPinnedObject(); }
        public IntPtr GetIndexPtr() { return gcIndices.AddrOfPinnedObject(); }

        public void SetVertices(Vector3[] vertices, Matrix4x4 transform)
        {
            this.vertices = new Vector3[vertices.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                this.vertices[i] = transform.MultiplyPoint(vertices[i]);
            }
        }
        public void SetVertices(Vector3[] vertices) { this.vertices = vertices; }

        public void SetTexCoords(Vector2[] texCoords) { this.texCoords = texCoords; }

        public void SetIndices(int[] indices) { this.indices = indices; }

        public void Transform(Matrix4x4 transform)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = transform.MultiplyPoint(vertices[i]);
            }
        }

        /// <summary>
        /// Construct the geometry with given information
        /// </summary>
        public void SetData(GeometryInfo info)
        {
            vertices = new Vector3[info.vertexCount];
            indices = new int[info.cellCount * typeToNumPts[info.type]];
            type = info.type;
        }

        /// <summary>
        /// Must be pinned for valid buffer pointers.
        /// Buffer ptrs become invalid when Geometry object returning it is deleted 
        /// </summary>
        /// <returns>Geometry as a primitive struct</returns>
        public PrimitiveGeometry GetPrimGeometry()
        {
            PrimitiveGeometry primMesh = new PrimitiveGeometry();
            primMesh.info.vertexCount = vertices.Length;
            primMesh.info.cellCount = indices.Length / typeToNumPts[type];
            primMesh.info.type = type;
            primMesh.vertexBufferPtr = GetVertexPtr();
            primMesh.indexBufferPtr = GetIndexPtr();
            return primMesh;
        }

        /// <summary>
        /// Extracts the surface of a volume mesh, null if not volume
        /// </summary>
        /// <param name="reverse">reverse winding of extracted surface</param>
        public Mesh ExtractSurfaceMesh(bool reverse = false)
        {
            if (!IsVolume)
                return null;

            int volHandle = ExportGeometry(this);
            int surfHandle = GeometryUtility.extractSurfaceMesh(volHandle, reverse);
            deleteGeometry(volHandle);
            Mesh results = GeometryToMesh(ImportGeometry(surfHandle));
            deleteGeometry(surfHandle);

            return results;
        }

        /// <summary>
        /// Converts Unity Mesh into Imstk Geometry
        /// </summary>
        public static Geometry MeshToGeometry(Mesh mesh)
        {
            Geometry geometry = CreateInstance<Geometry>();
            geometry.name = mesh.name;
            geometry.SetIndices(mesh.triangles);
            geometry.SetVertices(mesh.vertices);
            geometry.type = meshTopologyToGeomType[mesh.GetTopology(0)];
            return geometry;
        }
        /// <summary>
        /// Converts Unity Mesh into Imstk Geometry
        /// </summary>
        public static Geometry MeshToGeometry(Mesh mesh, Matrix4x4 transform)
        {
            Geometry geometry = CreateInstance<Geometry>();
            geometry.name = mesh.name;

            geometry.indices = new int[mesh.triangles.Length];
            mesh.triangles.CopyTo(geometry.indices, 0);

            geometry.texCoords = new Vector2[mesh.uv.Length];
            mesh.uv.CopyTo(geometry.texCoords, 0);

            geometry.SetVertices(mesh.vertices, transform);

            geometry.type = meshTopologyToGeomType[mesh.GetTopology(0)];
            return geometry;
        }

        /// <summary>
        /// Converts Geometry to unity mesh
        /// </summary>
        /// <returns>Unity Mesh, null if not possible</returns>
        public static Mesh GeometryToMesh(Geometry geometry)
        {
            if (geometry.IsVolume)
                return null;
            Mesh results = new Mesh();
            results.name = geometry.name;
            results.SetVertices(geometry.vertices);
            results.SetTriangles(geometry.indices, 0);
            if (geometry.texCoords.Length > 0)
                results.SetUVs(0, geometry.texCoords);
            results.RecalculateBounds();
            results.RecalculateNormals();
            // Set topology?
            return results;
        }

        /// <summary>
        /// Imports the geometry at the handle into am Imstk Geometry object
        /// </summary>
        public static Geometry ImportGeometry(int objHandle)
        {
            Geometry geometry = CreateInstance<Geometry>();
            geometry.SetData(getGeometryInfo(objHandle)); // Allocate the required buffers
            geometry.Pin(); // Pin the buffers
            exportGeometry(objHandle, geometry.GetPrimGeometry());
            geometry.Free();

            return geometry;
        }

        /// <summary>
        /// Exports the geometry to the c api
        /// </summary>
        /// <returns>The handle of the object in the c api</returns>
        public static int ExportGeometry(Geometry geometry)
        {
            geometry.Pin();
            int objHandle = genGeometryFromPrimitiveGeometry(geometry.GetPrimGeometry());
            geometry.Free();
            return objHandle;
        }


        /// <summary>
        /// Creates a new geometry and returns the handle
        /// </summary>
        /// <returns>Handle of the geometry in the c api</returns>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern int genGeometry(GeometryType type);

        /// <summary>
        /// Creates a new geometry with the given handle, returns if suceeded or not,
        /// fails if there already exists another geometry with that handle.
        /// </summary>
        /// <returns>Status, true if successfully created, false if not (handle already exists?)</returns>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern bool genGeometryWithHandle(GeometryType type, int objHandle);

        /// <summary>
        /// Creates a new geometry from the given primitive geometry
        /// </summary>
        /// <returns>Handle of the geometry in the c api</returns>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern int genGeometryFromPrimitiveGeometry(PrimitiveGeometry primGeometry);

        /// <summary>
        /// Returns various info about the geometry
        /// </summary>
        /// <returns>Struct of various info about the geometry</returns>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern GeometryInfo getGeometryInfo(int objHandle);

        /// <summary>
        /// Fills the provided primGeometry buffers with the geometry from the geometry object
        /// at the handle. This method is private, use the object one
        /// </summary>
        [DllImport(PInvoke.ImstkUnityLibName)]
        private static extern void exportGeometry(int objHandle, PrimitiveGeometry primGeometry);

        /// <summary>
        /// Deletes the geometry at the handle
        /// </summary>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void deleteGeometry(int objHandle);
    }

    public class GeometryUtility
    {
        /// <summary>
        /// Fills a mask telling whether points are inside/outside of a surface
        /// </summary>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void testEnclosedPoints(int surfHandle, int pointSetHandle, IntPtr isInside);

        /// <summary>
        /// Extracts surface mesh from volume
        /// </summary>
        /// <returns>Handle of the geometry in the c api</returns>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern int extractSurfaceMesh(int volHandle, bool flipNormals);

        /// <summary>
        /// Reverse the winding of a SurfaceMesh
        /// </summary>
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void reverseSurfaceMesh(int surfHandle);
    }
}