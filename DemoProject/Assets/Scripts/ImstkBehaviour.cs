﻿using UnityEngine;

namespace Imstk
{
    /// <summary>
    /// This is the base class of Imstk scripts. Scripts of this type
    /// are executed after SimulationManager awake, before SimulationManager
    /// start, and beofre SimulationManager destroy.
    /// </summary>
    public abstract class ImstkBehaviour : MonoBehaviour
    {
        public virtual void ImstkAwake() { }

        public virtual void ImstkStart() { }

        /// <summary>
        /// Called on the native thread after scene step
        /// </summary>
        public virtual void ImstkNativeUpdate() { }

        public virtual void ImstkDestroy() { }
    }
}
