﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    public enum PbdFemMaterialType
    {
        Linear,
        Corotation,
        StVK,
        NeoHookean
    };

    public struct PbdParams
    {
        public double distanceStiffness;
        public double dihedralStiffness;
        public double areaStiffness;
        public double volumeStiffness;
        public double youngsModulus;
        public double possionsRatio;
        public double mu;
        public double lambda;
        public double uniformMassValue;
        public Vector3 gravityAccel;
        public int numIterations;
        public int numCollisionIterations;
        public double contactStiffness;
        public double proximity;
        public double dt;
        public PbdFemMaterialType materialType;
        public IntPtr fixedIndices;
        public int fixedIndexCount;
    }

    [AddComponentMenu("Imstk/PbdModel")]
    public class PbdModel : DynamicalModel
    {
        public double distanceStiffness = 0.5;
        public bool useDistanceConstraint = true;

        public double dihedralStiffness = 0.3;
        public bool useDihedralConstraint = true;

        public double areaStiffness = 0.2;
        public bool useAreaConstraint = true;

        public double volumeStiffness = 0.5;
        public bool useVolumeConstraint = false;

        public double youngsModulus = 100.0;
        public double possionsRatio = 0.3;
        public double mu = 0.0;
        public double lambda = 0.0;
        public bool useYoungsModulus = true;
        public bool useFEMConstraint = true;
        public PbdFemMaterialType materialType = PbdFemMaterialType.StVK;

        public double contactStiffness = 0.5;
        public double proximity = 0.1;

        public double uniformMassValue = 1.0;
        public Vector3 gravityAccel = new Vector3(0.0f, -9.81f, 0.0f);
        public int numIterations = 5;
        public int numCollisionIterations = 3;

        public bool useRealtime = false;
        public double dt = 0.01;
        public HashSet<int> fixedIndices = new HashSet<int>();

        protected override void InitObject() { initPbdObject(GetHandle()); }

        protected override void Configure()
        {
            PbdParams pbdParams = new PbdParams();
            if (useDistanceConstraint)
                pbdParams.distanceStiffness = distanceStiffness;
            else
                pbdParams.distanceStiffness = -1.0;

            if (useDihedralConstraint)
                pbdParams.dihedralStiffness = dihedralStiffness;
            else
                pbdParams.dihedralStiffness = -1.0;

            if (useAreaConstraint)
                pbdParams.areaStiffness = areaStiffness;
            else
                pbdParams.areaStiffness = -1.0;

            if (useVolumeConstraint)
                pbdParams.volumeStiffness = volumeStiffness;
            else
                pbdParams.volumeStiffness = -1.0;

            if (useFEMConstraint)
            {
                if (useYoungsModulus)
                {
                    pbdParams.possionsRatio = possionsRatio;
                    pbdParams.youngsModulus = youngsModulus;
                    pbdParams.mu = 0.0;
                    pbdParams.lambda = 0.0;
                }
                else
                {
                    pbdParams.possionsRatio = 0.0;
                    pbdParams.youngsModulus = 0.0;
                    pbdParams.mu = mu;
                    pbdParams.lambda = lambda;
                }
            }
            else
                pbdParams.possionsRatio = pbdParams.youngsModulus = pbdParams.mu = pbdParams.lambda = -1.0;

            if (useRealtime)
                pbdParams.dt = 0.0;
            else
                pbdParams.dt = dt;

            pbdParams.materialType = materialType;
            pbdParams.uniformMassValue = uniformMassValue;
            pbdParams.gravityAccel = gravityAccel;
            pbdParams.numIterations = numIterations;
            pbdParams.numCollisionIterations = numCollisionIterations;
            pbdParams.contactStiffness = contactStiffness;
            pbdParams.proximity = proximity;

            pbdParams.fixedIndexCount = fixedIndices.Count;
            int[] fixedIndexArray = new int[fixedIndices.Count];
            fixedIndices.CopyTo(fixedIndexArray);
            GCHandle gcFixedIndices = GCHandle.Alloc(fixedIndexArray, GCHandleType.Pinned);
            pbdParams.fixedIndices = gcFixedIndices.AddrOfPinnedObject();
            configurePbdObject(GetHandle(), pbdParams);
            gcFixedIndices.Free();
        }

        protected override void ProcessBoundaryConditions(BoundaryCondition[] conditions)
        {
            // Pbd uses fixed vertices for BC, this is what we will compute
            fixedIndices.Clear();
            if (conditions.Length == 0)
                return;

            // For every BC test intersection to find fixed vertices
            foreach (BoundaryCondition condition in conditions)
            {
                // Create the surface mesh the points will be tested if inside of
                MeshFilter meshFilter = condition.bcObj.GetComponent<MeshFilter>();
                Transform transform = condition.bcObj.GetComponent<Transform>();
                if (meshFilter == null)
                    continue;
                // Convert unity to imstk geometry
                Geometry bcGeometry = Geometry.MeshToGeometry(meshFilter.sharedMesh, transform.localToWorldMatrix);
                // Export to c api
                int boundaryGeometryHandle = Geometry.ExportGeometry(bcGeometry);

                // Create an array/mask for which points are/aren't inside the surface
                bool[] isInside = new bool[physicsGeometry.vertices.Length];
                GCHandle gcIsInside = GCHandle.Alloc(isInside, GCHandleType.Pinned);
                GeometryUtility.testEnclosedPoints(boundaryGeometryHandle, physicsGeometryHandle, gcIsInside.AddrOfPinnedObject());
                for (int i = 0; i < isInside.Length; i++)
                {
                    if (isInside[i])
                        fixedIndices.Add(i);
                }
                gcIsInside.Free();
                Geometry.deleteGeometry(boundaryGeometryHandle);
            }
        }


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void configurePbdObject(int objectHandle, PbdParams pbdParams);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void initPbdObject(int objectHandle);
    }
}