﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    public enum FEMMethodType
    {
        StVK,
        Corotational,
        Linear,
        Invertible
    };
    public enum HyperElasticMaterialType
    {
        StVK,
        NeoHookean,
        MooneyRivlin,
        none
    };

    public struct FemParams
    {
        public FEMMethodType femMethodType;
        public HyperElasticMaterialType hyperElasticMaterialType;
        public double dampingMassCoefficient;
        public double dampingStiffnessCoefficient;
        public double dampingLaplacianCoefficient;
        public double deformationCompliance;
        public double gravityAccel;
        public double compressionResistance;
        public double inversionThreshold;
        public string configFilePath;
        public double dt;
        public IntPtr fixedIndices;
        public int fixedIndexCount;
    };

    [AddComponentMenu("Imstk/FemModel")]
    public class FemModel : DynamicalModel
    {
        public FEMMethodType femMethodType = FEMMethodType.Invertible;
        public HyperElasticMaterialType materialType = HyperElasticMaterialType.StVK;
        public double dampingMassCoefficient = 0.0;
        public double dampingStiffnessCoefficient = 0.0;
        public double dampingLaplacianCoefficient = 0.0;
        public double deformationCompliance = 1.0;
        public double gravityAccel = -9.81;
        public double compressionResistance = 500.0;
        public double inversionThreshold = 0.1;
        public string configFilePath = "";
        public bool useConfigFile = false;
        public bool useRealtime = false;
        public double dt = 0.01;
        public HashSet<int> fixedIndices = new HashSet<int>();

        protected override void InitObject() { initFemObject(GetHandle()); }

        protected override void Configure()
        {
            FemParams femParams = new FemParams();
            femParams.femMethodType = femMethodType;
            femParams.hyperElasticMaterialType = materialType;
            femParams.dampingMassCoefficient = dampingMassCoefficient;
            femParams.dampingStiffnessCoefficient = dampingStiffnessCoefficient;
            femParams.dampingLaplacianCoefficient = dampingLaplacianCoefficient;
            femParams.deformationCompliance = deformationCompliance;
            femParams.gravityAccel = -gravityAccel;
            femParams.compressionResistance = compressionResistance;
            femParams.inversionThreshold = inversionThreshold;

            if (useConfigFile)
                femParams.configFilePath = configFilePath;
            else
                femParams.configFilePath = "";

            if (useRealtime)
                femParams.dt = 0.0;
            else
                femParams.dt = dt;

            femParams.fixedIndexCount = fixedIndices.Count;
            int[] fixedIndexArray = new int[fixedIndices.Count];
            fixedIndices.CopyTo(fixedIndexArray);
            GCHandle gcFixedIndices = GCHandle.Alloc(fixedIndexArray, GCHandleType.Pinned);
            femParams.fixedIndices = gcFixedIndices.AddrOfPinnedObject();

            configureFemObject(GetHandle(), femParams);

            gcFixedIndices.Free();
        }

        protected override void ProcessBoundaryConditions(BoundaryCondition[] conditions)
        {
            // Pbd uses fixed vertices for BC, this is what we will compute
            fixedIndices.Clear();
            if (conditions.Length == 0)
                return;

            // For every BC test intersection to find fixed vertices
            foreach (BoundaryCondition condition in conditions)
            {
                // Create the surface mesh the points will be tested if inside of
                MeshFilter meshFilter = condition.bcObj.GetComponent<MeshFilter>();
                Transform transform = condition.bcObj.GetComponent<Transform>();
                if (meshFilter == null)
                    continue;
                // Convert unity to imstk geometry
                Geometry bcGeometry = Geometry.MeshToGeometry(meshFilter.sharedMesh, transform.localToWorldMatrix);
                // Export to c api
                int boundaryGeometryHandle = Geometry.ExportGeometry(bcGeometry);

                // Create an array/mask for which points are/aren't inside the surface
                bool[] isInside = new bool[physicsGeometry.vertices.Length];
                GCHandle gcIsInside = GCHandle.Alloc(isInside, GCHandleType.Pinned);
                GeometryUtility.testEnclosedPoints(boundaryGeometryHandle, physicsGeometryHandle, gcIsInside.AddrOfPinnedObject());
                for (int i = 0; i < isInside.Length; i++)
                {
                    if (isInside[i])
                        fixedIndices.Add(i);
                }
                gcIsInside.Free();
                Geometry.deleteGeometry(boundaryGeometryHandle);
            }
        }

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void configureFemObject(int objectHandle, FemParams femParams);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void initFemObject(int objectHandle);
    }
}