﻿using UnityEngine;

namespace Imstk
{
    public enum PhysicsGeometryUsage
    {
        USE_VISUAL_GEOMETRY,
        USE_OWN_GEOMETRY
    }

    // PhysicsGeometry is a component that can be attached to objects for mesh storage
    // It allows placement of both Unitys Mesh and ImstkMesh
    [AddComponentMenu("Imstk/PhysicsGeometry")]
    public class PhysicsGeometry : GeometryComponent
    {
        public PhysicsGeometryUsage usage = PhysicsGeometryUsage.USE_VISUAL_GEOMETRY;
    }
}