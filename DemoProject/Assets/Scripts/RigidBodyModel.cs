﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk
{
    public struct RigidParams
    {
        public double test;
    };

    [AddComponentMenu("Imstk/RigidBodyModel")]
    class RigidBodyModel : DynamicalModel
    {
        protected override void InitObject() { initRigidObject(GetHandle()); }

        protected override void Configure()
        {
            RigidParams rigidParams = new RigidParams();
            configureRigidObject(GetHandle(), rigidParams);
        }


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void configureRigidObject(int objectHandle, RigidParams pbdParams);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void initRigidObject(int objectHandle);
    }
}