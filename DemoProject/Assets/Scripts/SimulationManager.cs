﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Imstk
{
    public enum ModuleStatus
    {
        starting,
        running,
        pausing,
        paused,
        terminating,
        inactive
    };

    [AddComponentMenu("Imstk/SimulationManager")]
    [DefaultExecutionOrder(-99)]
    public class SimulationManager : MonoBehaviour
    {
        public static SimulationManager simManager;
        public delegate void PostUpdateCallbackHandler(EventArgs e);
        public static event PostUpdateCallbackHandler PostUpdateCallback;


        protected static void PostUpdateInvoke() { PostUpdateCallback?.Invoke(new EventArgs()); }

        // Sets the collision graph on the active scene
        public void setCollisionGraph(CollisionGraph graph)
        {
            // The collision graph uses instance ids, but the imported graph uses names
            // so lookup instance ids from names
            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            foreach (InteractionPair pair in graph.pairs)
            {
                GameObject obj1 = objects.Find(x => { return x.name == pair.object1Name; });
                GameObject obj2 = objects.Find(x => { return x.name == pair.object2Name; });
                if (obj1 == null || obj2 == null)
                    continue;
                addInteractionPair(obj1.GetInstanceID(), obj2.GetInstanceID(), (int)pair.interactionType, (int)pair.collisionDetectionType);
            }
        }


        private void Awake()
        {
            simManager = this;

            // Register reverse pinvoke function
            Logger.Register();

            setSimManagerPostUpdateCallback(Marshal.GetFunctionPointerForDelegate(new Action(PostUpdateInvoke)));

            // Get the settings
            ImstkSettings settings = ImstkSettings.Instance();
            if (settings.useOptimalNumberOfThreads)
                settings.numThreads = 0;

            // Create the simulation manager
            genSimManager(settings.numThreads);

            if (settings.useLogger)
                Logger.genLogger();

            // Manually do unity init because unity's lack of control
            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            foreach (GameObject obj in objects)
            {
                ImstkBehaviour[] behaviours = obj.GetComponents<ImstkBehaviour>();
                foreach (ImstkBehaviour behaviour in behaviours)
                {
                    behaviour.ImstkAwake();
                }
            }
        }

        private void Start()
        {
            // Before start is called setup the collision graph
            CollisionGraphData data = CollisionGraphData.Read(SceneManager.GetActiveScene().name + "_CollisionGraphData");
            if (data != null)
                setCollisionGraph(data.ToCollisionGraph());

            // Initialize the simulation manager (because objects are now added)
            initSimManager();

            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            foreach (GameObject obj in objects)
            {
                ImstkBehaviour[] behaviours = obj.GetComponents<ImstkBehaviour>();
                foreach (ImstkBehaviour behaviour in behaviours)
                {
                    behaviour.ImstkStart();
                }
            }

            startSimManager();
        }


        public void OnApplicationQuit()
        {
            // Manually clean up
            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            foreach (GameObject obj in objects)
            {
                ImstkBehaviour[] behaviours = obj.GetComponents<ImstkBehaviour>();
                foreach (ImstkBehaviour behaviour in behaviours)
                {
                    behaviour.ImstkDestroy();
                }
            }
            stopSimManager();
            deleteSimManager();
            Logger.deleteLogger();
        }


        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void genSimManager(int numThreads);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void initSimManager();

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void setSimManagerPostUpdateCallback(IntPtr funcPtr);

        [DllImport(PInvoke.ImstkUnityLibName)]
        protected static extern void addInteractionPair(int objAInstanceId, int objBInstanceId, int interactionType, int collisionDetectionType);

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void startSimManager();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void advanceFrame(double dt);

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern double getActiveSceneDt();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void pauseSimManager();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void resumeSimManager();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern ModuleStatus getSimManagerStatus();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void stopSimManager();

        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern void deleteSimManager();
    }
}