﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Imstk
{
    public enum NodeType
    {
        Collision,
        Model
    };

    [Serializable]
    public class NodeData
    {
        public string guid;
        public string title;
        public NodeType type;
        public Vector2 position;
    }

    [Serializable]
    public class CollisionNodeData : NodeData
    {
        public string object1Name;
        public string object2Name;
        public InteractionType interactionType;
        public CollisionDetectionType collisionDetectionType;
    }

    [Serializable]
    public class ModelNodeData : NodeData
    {
        public string objectName;
    }

    [Serializable]
    public class EdgeData
    {
        // Ids of the two nodes connected
        public string inputGuid;
        public string outputGuid;
        // The two ports used
        public string inputPortName;
        public string outputPortName;
    }

    // Serializable graph data for graph IO
    [Serializable]
    public class CollisionGraphData : ScriptableObject
    {
        // Unity doesn't support polymorphic serialization so keep nodes separate
        public List<CollisionNodeData> collisionNodes = new List<CollisionNodeData>();
        public List<ModelNodeData> modelNodes = new List<ModelNodeData>();
        public List<EdgeData> edges = new List<EdgeData>();
        public Vector3 viewPos = Vector3.zero;
        public Vector3 viewScale = Vector3.one;

        // Writes the CollisionGraph to Assets/Resources/filename.asset
        public void Write(string filename)
        {
            if (!AssetDatabase.IsValidFolder("Assets/Resources"))
                AssetDatabase.CreateFolder("Assets", "Resources");
            AssetDatabase.CreateAsset(this, $"Assets/Resources/{filename}.asset");
            AssetDatabase.SaveAssets();
        }

        // Computes the CollisionGraph from this CollisionGraphData
        public CollisionGraph ToCollisionGraph()
        {
            // To convert to collision graph we extract all valid InteractionPairs
            CollisionGraph results = new CollisionGraph();
            if (collisionNodes.Count == 0 || modelNodes.Count == 0 || edges.Count == 0)
                return results;

            // First construct something to lookup nodes by id
            Dictionary<string, NodeData> nodeMap = new Dictionary<string, NodeData>();
            Dictionary<NodeData, List<EdgeData>> connectivity = new Dictionary<NodeData, List<EdgeData>>();
            foreach (NodeData node in modelNodes)
            {
                nodeMap.Add(node.guid, node);
                connectivity.Add(node, new List<EdgeData>());
            }
            foreach (CollisionNodeData node in collisionNodes)
            {
                nodeMap.Add(node.guid, node);
                connectivity.Add(node, new List<EdgeData>());
            }

            // Now setup connectivity, map nodes to N edges
            foreach (EdgeData edge in edges)
            {
                // If the edge is not connected, skip
                if (edge.inputGuid == "" || edge.outputGuid == "")
                    continue;

                // Get & Add the nodes
                NodeData inputNode = nodeMap[edge.inputGuid];
                NodeData outputNode = nodeMap[edge.outputGuid];
                connectivity[inputNode].Add(edge);
                connectivity[outputNode].Add(edge);
            }

            // For every collision node
            foreach (CollisionNodeData node in collisionNodes)
            {
                // If the node doesn't two edges, skip
                if (connectivity[node].Count < 2)
                    continue;

                // Get the edges of the collision node
                EdgeData edge1 = connectivity[node][0];
                EdgeData edge2 = connectivity[node][1];

                // Get the input nodes from the edges
                ModelNodeData modelInputNode1 = nodeMap[edge1.inputGuid] as ModelNodeData;
                if (modelInputNode1 == null)
                    modelInputNode1 = nodeMap[edge1.outputGuid] as ModelNodeData;
                ModelNodeData modelInputNode2 = nodeMap[edge2.inputGuid] as ModelNodeData;
                if (modelInputNode2 == null)
                    modelInputNode2 = nodeMap[edge2.outputGuid] as ModelNodeData;

                // Create & Add the pair
                InteractionPair pair = new InteractionPair
                {
                    interactionType = node.interactionType,
                    collisionDetectionType = node.collisionDetectionType,
                    object1Name = modelInputNode1.objectName,
                    object2Name = modelInputNode2.objectName
                };
                results.pairs.Add(pair);
            }
            return results;
        }

        public static CollisionGraphData Read(string filename)
        {
            return Resources.Load(filename, typeof(CollisionGraphData)) as CollisionGraphData;
        }
    }
}