﻿using UnityEngine;
using UnityEditor.Experimental.AssetImporters;

namespace Imstk
{
    /// <summary>
    /// This class reads in various types of geometry utilized by Imstk
    /// </summary>
    [ScriptedImporter(1, new string[] { "vtk", "vtu", "vtp", "stl", "ply", "veg" })]
    public class GeometryImporter : ScriptedImporter
    {
        public bool flipSurfaceNormals = false;

        public override void OnImportAsset(AssetImportContext ctx)
        {
            string assetsPath = Application.dataPath;
            assetsPath = assetsPath.Replace("Assets", "");
            string filePath = assetsPath + ctx.assetPath;
            string fileName = System.IO.Path.GetFileNameWithoutExtension(ctx.assetPath);

            // Read the geometry
            int geomHandle = MeshIO.readGeometry(filePath);
            Geometry geometry = Geometry.ImportGeometry(geomHandle);
            geometry.name = fileName;

            // Now we have a couple of routes to go
            // If we can put this geometry into a Unity Mesh we should go ahead and do so
            if (geometry.type == GeometryType.PointSet ||
                geometry.type == GeometryType.LineMesh ||
                geometry.type == GeometryType.SurfaceMesh)
            {
                // Setup a default cube, remove BoxCollider, replace mesh
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                DestroyImmediate(obj.GetComponent<BoxCollider>());
                obj.name = fileName;

                if (geometry.type == GeometryType.SurfaceMesh && flipSurfaceNormals)
                {
                    GeometryUtility.reverseSurfaceMesh(geomHandle);
                    geometry = Geometry.ImportGeometry(geomHandle);
                    geometry.name = fileName;
                }

                Mesh mesh = new Mesh();
                mesh.name = obj.name + "_mesh";
                mesh.vertices = geometry.vertices;
                mesh.triangles = geometry.indices;
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                obj.GetComponent<MeshFilter>().sharedMesh = mesh;

                ctx.AddObjectToAsset(obj.name, obj);
                ctx.AddObjectToAsset(mesh.name, mesh);
                ctx.SetMainObject(obj);
            }
            // Otherwise if it's a volumetric type we will produce the volume and surface
            else if(geometry.type == GeometryType.TetrahedralMesh ||
                geometry.type == GeometryType.HexahedralMesh)
            {
                // Setup a default cube, remove BoxCollider, replace mesh, add Geometry (Imstk), replace Mesh (Unity)
                GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                DestroyImmediate(obj.GetComponent<BoxCollider>());
                obj.name = fileName;

                geometry.name = obj.name + "_mesh";

                Mesh surfMesh = geometry.ExtractSurfaceMesh(flipSurfaceNormals);
                surfMesh.name = geometry.name + "_surface";
                obj.GetComponent<MeshFilter>().sharedMesh = surfMesh;

                ctx.AddObjectToAsset(obj.name, obj);
                ctx.AddObjectToAsset(geometry.name, geometry);
                ctx.AddObjectToAsset(surfMesh.name, surfMesh);
                ctx.SetMainObject(obj);
            }

            // After reading we can go ahead and lose the unmanaged one
            // Unity will manage it from now on
            Geometry.deleteGeometry(geomHandle);
        }
    }
}