﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Imstk.Movement
{
    // Moves the set distance
    [AddComponentMenu("Imstk/PbdForwardMove")]
    public class PbdForwardMove : ImstkBehaviour
    {
        public double distanceToMove = 1.0;
        public double timeStep = 0.01;
        public bool useTransformDirection = false;
        public Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);

        private double t = 0.0;
        private int instanceId = -1;

        public override void ImstkStart()
        {
            SimulationManager.PostUpdateCallback += PostUpdate;
            instanceId = gameObject.GetInstanceID();
            // Get the direction to move
            if (useTransformDirection)
            {
                DynamicalModel model = gameObject.GetComponent<DynamicalModel>();
                if (model != null)
                    direction = (model.initTransform * new Vector3(0.0f, 0.0f, 1.0f)).normalized;
            }
        }

        public void PostUpdate(EventArgs e)
        {
            Vector3 dx = (float)(Math.Sin(t) * 0.5 * distanceToMove * timeStep) * direction;
            translateVertices(instanceId, dx);
            t += timeStep;
        }

        public override void ImstkDestroy()
        {
            SimulationManager.PostUpdateCallback -= PostUpdate;
        }

        [DllImport(PInvoke.ImstkUnityLibName)]
        private static extern void translateVertices(int objectHandle, Vector3 dx);
    }
}