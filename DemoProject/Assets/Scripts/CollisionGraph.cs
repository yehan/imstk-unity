﻿using System.Collections.Generic;

namespace Imstk
{
    public enum CollisionDetectionType
    {
        // Points to objects
        PointSetToSphere,
        PointSetToPlane,
        PointSetToCapsule,
        PointSetToSpherePicking,
        PointSetToSurfaceMesh,

        // Mesh to mesh (mesh to analytical object = mesh vertices to analytical object)
        SurfaceMeshToSurfaceMesh,
        SurfaceMeshToSurfaceMeshCCD,
        VolumeMeshToVolumeMesh,
        MeshToMeshBruteForce,

        // Analytical object to analytical object
        UnidirectionalPlaneToSphere,
        BidirectionalPlaneToSphere,
        SphereToCylinder,
        SphereToSphere
    };
    public enum InteractionType
    {
        PbdObjToPbdObjCollision,

        PbdObjToCollidingObjCollision,
        SphObjToCollidingObjCollision,
        FemObjToCollidingObjCollision,

        FemObjToCollidingObjPenaltyForce,
        FemObjToCollidingObjBoneDrilling,
        FemObjToCollidingObjNodalPicking
    };

    public class InteractionPair
    {
        public string object1Name;
        public string object2Name;
        public InteractionType interactionType;
        public CollisionDetectionType collisionDetectionType;
    }

    public class CollisionGraph
    {
        public List<InteractionPair> pairs = new List<InteractionPair>();

        public static Dictionary<string, CollisionDetectionType> collisionDetectionTypes = new Dictionary<string, CollisionDetectionType>()
        {
            { "PointSet To Sphere", CollisionDetectionType.PointSetToSphere },
            { "PointSet To Plane", CollisionDetectionType.PointSetToPlane },
            { "PointSet To Capsule", CollisionDetectionType.PointSetToCapsule },
            { "PointSet To SpherePicking", CollisionDetectionType.PointSetToSpherePicking },
            { "PointSet To SurfaceMesh", CollisionDetectionType.PointSetToSurfaceMesh },

            { "SurfaceMesh To SurfaceMesh", CollisionDetectionType.SurfaceMeshToSurfaceMesh },
            { "SurfaceMesh To SurfaceMesh CD", CollisionDetectionType.SurfaceMeshToSurfaceMeshCCD },
            { "VolumeMesh To VolumeMesh", CollisionDetectionType.VolumeMeshToVolumeMesh },
            { "Mesh To Mesh", CollisionDetectionType.MeshToMeshBruteForce },

            { "UnidirectionalPlane To Sphere", CollisionDetectionType.UnidirectionalPlaneToSphere },
            { "BidirectionalPlane To Sphere", CollisionDetectionType.BidirectionalPlaneToSphere },
            { "Sphere To Cylinder", CollisionDetectionType.SphereToCylinder },
            { "Sphere To Sphere", CollisionDetectionType.SphereToSphere },
        };
        public static Dictionary<string, InteractionType> interactionTypes = new Dictionary<string, InteractionType>()
        {
            { "PbdObj-To-PbdObj Collision", InteractionType.PbdObjToPbdObjCollision },

            { "PbdObj-To-CollidingObj Collision", InteractionType.PbdObjToCollidingObjCollision },
            { "SphObj-To-CollidingObj Collision", InteractionType.SphObjToCollidingObjCollision },
            { "FemObj-To-CollidingObj Collision", InteractionType.FemObjToCollidingObjCollision },

            { "FemObj-To-CollidingObj PenaltyForce", InteractionType.FemObjToCollidingObjPenaltyForce },
            { "FemObj-To-CollidingObj BoneDrilling", InteractionType.FemObjToCollidingObjBoneDrilling },
            { "FemObj-To-CollidingObj NodalPicking", InteractionType.FemObjToCollidingObjNodalPicking },
        };
    }
}