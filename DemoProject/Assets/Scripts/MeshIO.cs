﻿using System.Runtime.InteropServices;

namespace Imstk
{
    class MeshIO
    {
        ///
        /// \brief Generates and reads the geomery from the given file, returns handle to mesh
        ///
        [DllImport(PInvoke.ImstkUnityLibName)]
        public static extern int readGeometry(string filePath);
    }
}