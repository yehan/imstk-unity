﻿using UnityEngine;

namespace Imstk
{
    public enum CollisionGeometryUsage
    {
        USE_PHYSICS_GEOMETRY,
        USE_VISUAL_GEOMETRY,
        USE_OWN_GEOMETRY
    }

    // ColliderGeometry is a component that can be attached to objects for collision
    // It allows placement of both Unitys Mesh and ImstkMesh
    [AddComponentMenu("Imstk/CollisionGeometry")]
    public class CollisionGeometry : GeometryComponent
    {
        public CollisionGeometryUsage usage = CollisionGeometryUsage.USE_VISUAL_GEOMETRY;
    }
}