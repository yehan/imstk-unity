﻿using Imstk;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    /// <summary>
    /// A collision node specifies types required for collision between two objects
    /// </summary>
    class CollisionNode : CollisionGraphNode
    {
        PopupField<string> interactionTypeField = null;
        PopupField<string> collisionDetectionTypeField = null;

        public CollisionNode(string title, InteractionType interactionType, CollisionDetectionType collisionDetectionType)
        {
            data = new CollisionNodeData();
            CollisionNodeData collisionData = data as CollisionNodeData;
            SetData(NodeType.Collision, title);
            collisionData.interactionType = interactionType;
            collisionData.collisionDetectionType = collisionDetectionType;
            AddFields();
        }
        public CollisionNode(CollisionNodeData data)
        {
            title = data.title;
            this.data = data;
            AddFields();
        }

        protected void AddFields()
        {
            CollisionNodeData collisionData = data as CollisionNodeData;

            // Create a list of interaction type strings
            List<string> interactionTypeOptions = new List<string>();
            foreach (KeyValuePair<string, InteractionType> entry in CollisionGraph.interactionTypes)
            {
                interactionTypeOptions.Add(entry.Key);
            }
            // Setup and add the interaction field
            interactionTypeField = new PopupField<string>(interactionTypeOptions, (int)collisionData.interactionType);
            interactionTypeField.RegisterCallback((ChangeEvent<string> e) =>
            {
                (data as CollisionNodeData).interactionType = CollisionGraph.interactionTypes[interactionTypeField.value];
            });
            Add(interactionTypeField);

            // Create a list of collision detection type strings
            List<string> collisionDetectionOptions = new List<string>();
            foreach (KeyValuePair<string, CollisionDetectionType> entry in CollisionGraph.collisionDetectionTypes)
            {
                collisionDetectionOptions.Add(entry.Key);
            }
            // Setup and add the collision detection field
            collisionDetectionTypeField = new PopupField<string>(collisionDetectionOptions, (int)collisionData.collisionDetectionType);
            collisionDetectionTypeField.RegisterCallback((ChangeEvent<string> e) =>
            {
                (data as CollisionNodeData).collisionDetectionType = CollisionGraph.collisionDetectionTypes[collisionDetectionTypeField.value];
            });
            Add(collisionDetectionTypeField);
        }

        public override NodeData GetData()
        {
            CollisionNodeData collisionData = data as CollisionNodeData;
            collisionData.interactionType = GetInteractionType();
            collisionData.collisionDetectionType = GetCollisionDetectionType();
            return collisionData;
        }

        public InteractionType GetInteractionType() { return CollisionGraph.interactionTypes[interactionTypeField.text]; }
        public CollisionDetectionType GetCollisionDetectionType() { return CollisionGraph.collisionDetectionTypes[collisionDetectionTypeField.text]; }
    }
}