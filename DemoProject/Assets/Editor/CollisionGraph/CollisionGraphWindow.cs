﻿using Imstk;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    public class CollisionGraphWindow : EditorWindow
    {
        private CollisionGraphView graphView = null;
        private Toolbar toolbar = null;

        [MenuItem("Window/CollisionGraph")]
        public static void ShowWindow()
        {
            GetWindow<CollisionGraphWindow>("Collision Graph Editor");
        }

        void OnEnable()
        {
            AddGraphView();
            AddToolbar();
            OnFocus();
        }

        private void AddGraphView()
        {
            // If the view hasn't already been setup once
            graphView = new CollisionGraphView
            {
                name = "Collision Graph"
            };
            graphView.StretchToParentSize();
            rootVisualElement.Add(graphView);
        }

        private void AddToolbar()
        {
            ToolbarMenu colliderMenu = new ToolbarMenu();
            colliderMenu.text = "Create";

            // Adds a default collider node
            colliderMenu.menu.AppendAction("Collider", (DropdownMenuAction) => { graphView.CreateCollisionNode("Collider", InteractionType.PbdObjToPbdObjCollision, CollisionDetectionType.MeshToMeshBruteForce); });

            toolbar = new Toolbar();
            toolbar.Add(colliderMenu);
            rootVisualElement.Add(toolbar);
        }

        // Adds Nodes to the graph that are new
        void AddNewModelNodes()
        {
            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            List<Node> nodes = graphView.nodes.ToList();
            foreach (GameObject obj in objects)
            {
                // If model exists on this gameobject
                DynamicalModel model = obj.GetComponent<DynamicalModel>();
                if (model == null)
                    continue;
                bool found = false;
                foreach (CollisionGraphNode node in nodes)
                {
                    ModelNode modelNode = node as ModelNode;
                    if (modelNode != null && (modelNode.data as ModelNodeData).objectName == obj.name)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    graphView.CreateModelNode(obj.name);
            }
        }
        // Remove nodes from the graph that shouldn't exist anymore
        void RemoveOldModelNodes()
        {
            List<GameObject> objects = FindObjectsOfType<GameObject>().ToList();
            List<Node> nodes = graphView.nodes.ToList();

            // Remove any Node for a GameObject no longer present (that is a DynamicalModel & doesn't exist in the scene)
            foreach (CollisionGraphNode node in nodes)
            {
                if (node.data.type == NodeType.Model &&
                    objects.Find((GameObject obj) => { return obj.name == (node.data as ModelNodeData).objectName; }) == null)
                    graphView.RemoveNode(node);
            }
        }

        void OnFocus()
        {
            if (graphView == null)
                return;

            // Read the graph for the scene
            CollisionGraphData data = CollisionGraphData.Read(SceneManager.GetActiveScene().name + "_CollisionGraphData");
            // If the graph data doesn't exist then create new data
            if (data == null)
            {
                graphView.ClearView();
                graphView.schedule.Execute(() => { AddNewModelNodes(); });
            }
            else
            {
                // Otherwise set the loaded data and update with new changes
                graphView.schedule.Execute(() =>
                {
                    graphView.SetCollisionGraphData(data);
                    RemoveOldModelNodes();
                    AddNewModelNodes();
                });
            }
        }

        void OnLostFocus()
        {
            if (graphView == null)
                return;

            // Write the graph on losing focus
            graphView.GetCollisionGraphData().Write(SceneManager.GetActiveScene().name + "_CollisionGraphData");
        }

        void OnDisable()
        {
            rootVisualElement.Remove(graphView);
            graphView = null;
            rootVisualElement.Remove(toolbar);
            toolbar = null;
        }
    }
}