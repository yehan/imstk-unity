﻿using Imstk;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    //class MeshPreviewElement : ImmediateModeElement
    //{
    //    public GameObject previewObj = null;
    //    PreviewRenderUtility renUtil = null;
    //    Rect rect;

    //    public MeshPreviewElement()
    //    {
    //        renUtil = new PreviewRenderUtility();
    //        rect = new Rect(0.0f, 0.0f, 100.0f, 100.0f);
    //    }

    //    ~MeshPreviewElement()
    //    {
    //        renUtil.Cleanup();
    //    }

    //    protected override void ImmediateRepaint()
    //    {
    //        if (previewObj == null)
    //            return;

    //        MeshRenderer meshRen = previewObj.GetComponent<MeshRenderer>();
    //        MeshFilter meshFilter = previewObj.GetComponent<MeshFilter>();

    //        renUtil.BeginPreview(rect, GUIStyle.none);
    //        renUtil.camera.transform.Translate(0.0f, 0.0f, 20.0f);
    //        renUtil.camera.transform.LookAt(meshFilter.sharedMesh.bounds.center, Vector3.up);

    //        //renUtil.AddSingleGO(previewObj);
    //        renUtil.DrawMesh(meshFilter.sharedMesh, Vector3.one, Quaternion.identity, meshRen.sharedMaterial, 0);

    //        renUtil.EndAndDrawPreview(rect);
    //    }
    //};

    class ModelNode : CollisionGraphNode
    {
        public ModelNode(string objectName)
        {
            data = new ModelNodeData();
            SetData(NodeType.Model, objectName);
            (data as ModelNodeData).objectName = objectName;

            //GameObject[] gameObjects = GameObject.FindObjectsOfType<GameObject>();
            //GameObject renderObject = null;
            //for (int i = 0; i < gameObjects.Length; i++)
            //{
            //    if (gameObjects[i].name == objectName)
            //        renderObject = gameObjects[i];
            //}
            //MeshPreviewElement meshPreview = new MeshPreviewElement();
            //meshPreview.previewObj = renderObject;
            //Add(meshPreview);
        }

        // Create node from serialized data
        public ModelNode(ModelNodeData data)
        {
            title = data.title;
            this.data = data;
        }
    }
}