﻿using Imstk;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace ImstkEditor
{
    class CollisionGraphView : GraphView
    {
        public CollisionGraphView()
        {
            // Initialize controls
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new FreehandSelector());

            // Block the user from deleting ModelNode's
            RegisterCallback((KeyDownEvent e) =>
            {
                if (e.keyCode == KeyCode.Delete && 
                    selection.Count > 0 &&
                    (selection.ElementAt(0) as ModelNode) != null)
                {
                    e.StopImmediatePropagation();
                    return;
                }
            });

            // Read in the background grid from a file
            GridBackground grid = new GridBackground();
            Insert(0, grid);
            grid.StretchToParentSize();
            styleSheets.Add(Resources.Load<StyleSheet>("CollisionGraphStyle"));
        }


        // Gets the CollisionGraphData object from the GraphView
        public CollisionGraphData GetCollisionGraphData()
        {
            // Extract collision graph data from the graph
            CollisionGraphData results = ScriptableObject.CreateInstance<CollisionGraphData>();
            List<Node> nodes = this.nodes.ToList();
            List<Edge> edges = this.edges.ToList();

            // Get NodeDatas from nodes
            List<CollisionNodeData> collisionNodeDatas = results.collisionNodes;
            List<ModelNodeData> modelNodeDatas = results.modelNodes;
            foreach (Node node in nodes)
            {
                CollisionGraphNode colGraphNode = node as CollisionGraphNode;
                colGraphNode.data.position = new Vector2(node.GetPosition().x, node.GetPosition().y);
                if (colGraphNode.data.type == NodeType.Collision)
                    collisionNodeDatas.Add(colGraphNode.data as CollisionNodeData);
                else if (colGraphNode.data.type == NodeType.Model)
                    modelNodeDatas.Add(colGraphNode.data as ModelNodeData);
            }

            // Compute all the edges
            List<EdgeData> edgeDatas = results.edges;
            foreach (Edge edge in edges)
            {
                EdgeData data = new EdgeData();
                if (edge.input != null)
                {
                    data.inputGuid = (edge.input.node as CollisionGraphNode).data.guid;
                    data.inputPortName = edge.input.portName;
                }
                if (edge.output != null)
                {
                    data.outputGuid = (edge.output.node as CollisionGraphNode).data.guid;
                    data.outputPortName = edge.output.portName;
                }
                edgeDatas.Add(data);
            }

            results.viewPos = viewTransform.position;
            results.viewScale = viewTransform.scale;
            return results;
        }

        // Sets the CollisionGraphData object on the GraphView
        public void SetCollisionGraphData(CollisionGraphData data)
        {
            ClearView();

            // Add every node to this dictionary by name
            Dictionary<string, CollisionGraphNode> nodeMap = new Dictionary<string, CollisionGraphNode>();
            foreach (ModelNodeData nodeData in data.modelNodes)
            {
                nodeMap.Add(nodeData.guid, CreateModelNode(nodeData));
            }
            foreach (CollisionNodeData nodeData in data.collisionNodes)
            {
                nodeMap.Add(nodeData.guid, CreateCollisionNode(nodeData));
            }

            // Add all the edges
            foreach (EdgeData edgeData in data.edges)
            {
                if (!nodeMap.ContainsKey(edgeData.inputGuid))
                    continue;
                if (!nodeMap.ContainsKey(edgeData.outputGuid))
                    continue;
                CollisionGraphNode inputNode = nodeMap[edgeData.inputGuid];
                CollisionGraphNode outputNode = nodeMap[edgeData.outputGuid];

                // The inputNode should have an output port with name
                Port inputPort = inputNode.ports.Find((Port port) => { return port.portName == edgeData.inputPortName; });
                // The outputNode should have an input port with name
                Port outputPort = outputNode.ports.Find((Port port) => { return port.portName == edgeData.outputPortName; });

                if (inputPort == null || outputPort == null)
                    continue;

                // Create edge with input and output ports
                Edge edge = new Edge()
                {
                    output = outputPort,
                    input = inputPort
                };
                edge.input.Connect(edge);
                edge.output.Connect(edge);
                Add(edge);
            }

            UpdateViewTransform(data.viewPos, data.viewScale);
        }

        // Removes all graph elements
        public void ClearView()
        {
            List<GraphElement> elements = graphElements.ToList();
            for (int i = 0; i < elements.Count; i++)
            {
                RemoveElement(elements[i]);
            }
        }

        // Removes node and its edges
        public void RemoveNode(CollisionGraphNode node)
        {
            // Remove edges connected to this node
            List<Edge> edgeList = edges.ToList();
            foreach (Edge edge in edgeList)
            {
                if (edge.input.node == node || edge.output.node == node)
                    RemoveElement(edge);
            }

            RemoveElement(node);
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            // Set which ports are compatible
            List<Port> compatiblePorts = new List<Port>();

            ports.ForEach((port) =>
            {
                if (startPort != port && startPort.node != port.node)
                    compatiblePorts.Add(port);
            });
            return compatiblePorts;
        }

        // Computes the bounds of the nodes
        public Rect GetNodeBounds()
        {
            List<GraphElement> elements = graphElements.ToList();
            if (nodes.ToList().Count == 0)
                return new Rect(0.0f, 0.0f, 0.0f, 0.0f);

            // Compute bounding box of current nodes and place underneath
            float[] bounds = { float.MaxValue, float.MinValue, float.MaxValue, float.MinValue };
            for (int i = 0; i < elements.Count; i++)
            {
                Rect elementRect = elements[i].GetPosition();

                //Debug.Log(elementRect.x.ToString() + " " + elementRect.y.ToString());
                float[] elementBounds = { elementRect.x, elementRect.x + elementRect.width, elementRect.y, elementRect.y + elementRect.height };
                if (elementBounds[0] < bounds[0])
                    bounds[0] = elementBounds[0];
                if (elementBounds[1] > bounds[1])
                    bounds[1] = elementBounds[1];
                if (elementBounds[2] < bounds[2])
                    bounds[2] = elementBounds[2];
                if (elementBounds[3] > bounds[3])
                    bounds[3] = elementBounds[3];
            }
            return new Rect(bounds[0], bounds[2], bounds[1] - bounds[0], bounds[3] - bounds[2]);
        }


        public CollisionNode CreateCollisionNode(CollisionNodeData data)
        {
            CollisionNode node = new CollisionNode(data);
            AddCollisionNode(node);
            return node;
        }
        public CollisionNode CreateCollisionNode(string title, InteractionType interactionType, CollisionDetectionType collisionDetectionType, Vector2? pos = null)
        {
            CollisionNode node = new CollisionNode(title, interactionType, collisionDetectionType);
            if (pos == null)
                node.data.position = Vector2.zero;
            else
                node.data.position = pos.Value;
            AddCollisionNode(node);
            return node;
        }
        public void AddCollisionNode(CollisionNode node)
        {
            Port inputPort1 = node.InstantiatePort(
                Orientation.Horizontal,
                Direction.Input,
                Port.Capacity.Single,
                typeof(float));
            inputPort1.name = inputPort1.portName = "Input A";
            node.inputContainer.Add(inputPort1);

            Port inputPort2 = node.InstantiatePort(
                Orientation.Horizontal,
                Direction.Input,
                Port.Capacity.Single,
                typeof(float));
            inputPort2.name = inputPort2.portName = "Input B";
            node.inputContainer.Add(inputPort2);

            node.RefreshExpandedState();
            node.RefreshPorts();
            node.SetPosition(new Rect(node.data.position, new Vector2(100.0f, 150.0f)));
            AddElement(node);
        }

        // Create node from serialized data
        public ModelNode CreateModelNode(ModelNodeData data)
        {
            ModelNode node = new ModelNode(data);
            AddModelNode(node);
            return node;
        }
        public ModelNode CreateModelNode(string objectName, Vector2? pos = null)
        {
            ModelNode node = new ModelNode(objectName);
            if (pos == null)
                node.data.position = Vector2.zero;
            else
                node.data.position = pos.Value;
            AddModelNode(node);
            return node;
        }
        public void AddModelNode(ModelNode node)
        {
            Port outputPort = node.InstantiatePort(
                Orientation.Horizontal,
                Direction.Output,
                Port.Capacity.Multi,
                typeof(float));
            outputPort.name = "Output";
            node.inputContainer.Add(outputPort);

            node.RefreshExpandedState();
            node.RefreshPorts();
            node.SetPosition(new Rect(node.data.position, new Vector2(100.0f, 150.0f)));
            AddElement(node);
        }
    }
}