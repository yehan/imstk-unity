﻿using Imstk;
using UnityEngine;
using UnityEditor;

namespace ImstkEditor
{
    [CustomEditor(typeof(PhysicsGeometry))]
    public class PhysicsGeometryEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            PhysicsGeometry physicsGeometry = target as PhysicsGeometry;
            EditorGUI.BeginChangeCheck();

            PhysicsGeometryUsage usage = (PhysicsGeometryUsage)EditorGUILayout.EnumPopup("Usage", physicsGeometry.usage);
            Object mesh = physicsGeometry.mesh;
            if (usage == PhysicsGeometryUsage.USE_OWN_GEOMETRY)
                mesh = EditorGUILayout.ObjectField("Mesh", physicsGeometry.mesh, typeof(Object), true);

            // Record the new states
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(physicsGeometry, "Change of PhysicsGeometry");
                physicsGeometry.usage = usage;

                if (mesh != null)
                {
                    if (mesh.GetType() == typeof(Geometry))
                        physicsGeometry.SetMesh(mesh as Geometry);
                    else if (mesh.GetType() == typeof(Mesh))
                        physicsGeometry.SetMesh(mesh as Mesh);
                    else
                        Debug.LogWarning("Object must be a Unity Mesh or Imstk Geometry");
                }
            }
        }
    }
}