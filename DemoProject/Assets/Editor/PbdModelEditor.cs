﻿using Imstk;
using UnityEngine;
using UnityEditor;

namespace ImstkEditor
{
    [CustomEditor(typeof(PbdModel))]
    public class PbdModelEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            PbdModel modelScript = target as PbdModel;
            EditorGUI.BeginChangeCheck();

            bool useDistanceConstraint = EditorGUILayout.Toggle("Distance Stiffness", modelScript.useDistanceConstraint);
            double distanceStiffness = modelScript.distanceStiffness;
            if (useDistanceConstraint)
                distanceStiffness = EditorGUILayout.Slider((float)modelScript.distanceStiffness, 0.0f, 1.0f);

            bool useDihedralConstraint = EditorGUILayout.Toggle("Dihedral Stiffness", modelScript.useDihedralConstraint);
            double dihedralStiffness = modelScript.dihedralStiffness;
            if (useDihedralConstraint)
                dihedralStiffness = EditorGUILayout.Slider((float)modelScript.dihedralStiffness, 0.0f, 1.0f);

            bool useAreaConstraint = EditorGUILayout.Toggle("Area Stiffness", modelScript.useAreaConstraint);
            double areaStiffness = modelScript.areaStiffness;
            if (useAreaConstraint)
                areaStiffness = EditorGUILayout.Slider((float)modelScript.areaStiffness, 0.0f, 1.0f);

            bool useVolumeConstraint = EditorGUILayout.Toggle("Volume Stiffness", modelScript.useVolumeConstraint);
            double volumeStiffness = modelScript.volumeStiffness;
            if (useVolumeConstraint)
                volumeStiffness = EditorGUILayout.Slider((float)modelScript.volumeStiffness, 0.0f, 1.0f);

            bool useFEMConstraint = EditorGUILayout.Toggle("FEM", modelScript.useFEMConstraint);
            double youngsModulus = modelScript.youngsModulus;
            double possionsRatio = modelScript.possionsRatio;
            bool useYoungsModulus = modelScript.useYoungsModulus;
            double mu = modelScript.mu;
            double lambda = modelScript.lambda;
            PbdFemMaterialType materialType = modelScript.materialType;
            if (useFEMConstraint)
            {
                useYoungsModulus = EditorGUILayout.Toggle("Use Youngs Modulus", modelScript.useYoungsModulus);
                if (useYoungsModulus)
                {
                    youngsModulus = EditorGUILayout.DoubleField("Youngs Modulus", modelScript.youngsModulus);
                    possionsRatio = EditorGUILayout.DoubleField("Possions Ratio", modelScript.possionsRatio);
                }
                else
                {
                    mu = EditorGUILayout.DoubleField("Mu", modelScript.mu);
                    lambda = EditorGUILayout.DoubleField("Lambda", modelScript.lambda);
                }
                materialType = (PbdFemMaterialType)EditorGUILayout.EnumPopup("Material Type", modelScript.materialType);
            }

            bool useRealtime = EditorGUILayout.Toggle("Use Realtime", modelScript.useRealtime);
            double dt = modelScript.dt;
            if (!useRealtime)
                dt = EditorGUILayout.DoubleField("Timestep", modelScript.dt);

            double uniformMassValue = EditorGUILayout.DoubleField("Uniform Mass Value", modelScript.uniformMassValue);
            Vector3 gravityAccel = EditorGUILayout.Vector3Field("Gravity Accel", modelScript.gravityAccel);
            int numIterations = EditorGUILayout.IntField("# Iterations", modelScript.numIterations);
            int numCollisionIterations = EditorGUILayout.IntField("# Collision Iterations", modelScript.numCollisionIterations);
            double contactStiffness = EditorGUILayout.DoubleField("Contact Stiffness", modelScript.contactStiffness);
            double proximity = EditorGUILayout.DoubleField("Proximity", modelScript.proximity);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RegisterCompleteObjectUndo(modelScript, "Change of Parameters");
                modelScript.useDistanceConstraint = useDistanceConstraint;
                modelScript.distanceStiffness = distanceStiffness;
                modelScript.useDihedralConstraint = useDihedralConstraint;
                modelScript.dihedralStiffness = dihedralStiffness;
                modelScript.useAreaConstraint = useAreaConstraint;
                modelScript.areaStiffness = areaStiffness;
                modelScript.useVolumeConstraint = useVolumeConstraint;
                modelScript.volumeStiffness = volumeStiffness;
                modelScript.useFEMConstraint = useFEMConstraint;
                modelScript.youngsModulus = youngsModulus;
                modelScript.possionsRatio = possionsRatio;
                modelScript.mu = mu;
                modelScript.lambda = lambda;
                modelScript.useRealtime = useRealtime;
                modelScript.dt = dt;
                modelScript.uniformMassValue = uniformMassValue;
                modelScript.gravityAccel = gravityAccel;
                modelScript.numIterations = numIterations;
                modelScript.numCollisionIterations = numCollisionIterations;
                modelScript.contactStiffness = contactStiffness;
                modelScript.proximity = proximity;
                modelScript.useYoungsModulus = useYoungsModulus;
                modelScript.materialType = materialType;
            }
        }
    }
}