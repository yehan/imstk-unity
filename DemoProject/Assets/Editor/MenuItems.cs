﻿using Imstk;
using UnityEngine;
using UnityEditor;

namespace ImstkEditor
{
    public class MenuItems : Editor
    {
        [MenuItem("GameObject/Imstk/PbdVolume")]
        [MenuItem("CONTEXT/Imstk/PbdVolume")]
        private static void CreatePbdVolume()
        {
            GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            DestroyImmediate(newObj.GetComponent<Collider>());

            PbdModel model = newObj.AddComponent<PbdModel>();
            model.useDistanceConstraint = false;
            model.useAreaConstraint = false;
            model.useDihedralConstraint = false;
            model.useVolumeConstraint = false;
            model.useFEMConstraint = true;

            PhysicsGeometry physicsGeometry = newObj.AddComponent<PhysicsGeometry>();
            physicsGeometry.usage = PhysicsGeometryUsage.USE_OWN_GEOMETRY;
            physicsGeometry.SetMesh(Utility.GetTetCubeMesh());

            CollisionGeometry collisionGeometry = newObj.AddComponent<CollisionGeometry>();
            collisionGeometry.usage = CollisionGeometryUsage.USE_VISUAL_GEOMETRY;
        }

        [MenuItem("GameObject/Imstk/PbdCloth")]
        [MenuItem("CONTEXT/Imstk/PbdCloth")]
        private static void CreatePbdCloth()
        {
            GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Plane);
            DestroyImmediate(newObj.GetComponent<Collider>());

            PbdModel model = newObj.AddComponent<PbdModel>();
            model.useDistanceConstraint = true;
            model.useDihedralConstraint = true;
            model.useAreaConstraint = false;
            model.useVolumeConstraint = false;
            model.useFEMConstraint = false;
            model.dt = 0.005;
            model.distanceStiffness = 0.02;
            model.dihedralStiffness = 0.01;

            MeshFilter filter = newObj.GetComponent<MeshFilter>();
            filter.sharedMesh = Geometry.GeometryToMesh(Utility.GetXYPlane(19, 19));
        }

        [MenuItem("GameObject/Imstk/FemVolume")]
        [MenuItem("CONTEXT/Imstk/FemVolume")]
        private static void CreateFemVolume()
        {
            GameObject newObj = new GameObject("FemObject");
            DestroyImmediate(newObj.GetComponent<Collider>());

            FemModel model = newObj.AddComponent<FemModel>();

            PhysicsGeometry physicsGeometry = newObj.AddComponent<PhysicsGeometry>();
            physicsGeometry.usage = PhysicsGeometryUsage.USE_OWN_GEOMETRY;
            physicsGeometry.SetMesh(Utility.GetTetCubeMesh());

            CollisionGeometry collisionGeometry = newObj.AddComponent<CollisionGeometry>();
            collisionGeometry.usage = CollisionGeometryUsage.USE_VISUAL_GEOMETRY;
        }

        [MenuItem("GameObject/Imstk/SimulationManager")]
        [MenuItem("CONTEXT/Imstk/SimulationManager")]
        private static void CreateSimulationManagerGameObject()
        {
            GameObject newObj = new GameObject("SimulationManager");
            newObj.AddComponent(typeof(SimulationManager));
        }
    }
}