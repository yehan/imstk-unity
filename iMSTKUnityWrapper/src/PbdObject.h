#pragma once
#include <imstkPbdFEMConstraint.h>

#include "CommonStructs.h"
#include "PInvoke.h"

struct PbdParams
{
    double distanceStiffness;
    double dihedralStiffness;
    double areaStiffness;
    double volumeStiffness;
    double youngsModulus;
    double possionRatio;
    double mu;
    double lambda;
    double uniformMassValue;
    Vec3f gravityAccel;
    int numIterations;
    int numCollisionIterations;
    double contactStiffness;
    double proximity;
    double dt;
    imstk::PbdFEMConstraint::MaterialType materialType;
    int* fixedIndices;
    int fixedIndexCount;
};

extern "C"
{
    ///
    /// \brief Sets the parameters of the specified PbdObject's model
    ///
    IMSTK_EXPORT void configurePbdObject(const int objectHandle, const PbdParams params);

    ///
    /// \brief Initializes a PbdObject with a PbdModel.
    ///
    IMSTK_EXPORT void initPbdObject(const int objectHandle);

    ///
    /// \brief Set a single vertex in the state
    ///
    IMSTK_EXPORT void setPbdStateVertex(const int objectHandle, const int index, Vec3f vertex);

    ///
    /// \brief Transforms all vertices of a pbd model
    ///
    IMSTK_EXPORT void translateVertices(const int objectHandle, Vec3f dx);

    ///
    /// \brief Get a single vertex from the state
    ///
    IMSTK_EXPORT Vec3f getPbdStateVertex(const int objectHandle, const int index);
}