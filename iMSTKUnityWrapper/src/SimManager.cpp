#include "SimManager.h"

#include <imstkCamera.h>
#include <imstkCollisionDetection.h>
#include <imstkCollisionGraph.h>
#include <imstkDynamicObject.h>
#include <imstkScene.h>
#include <imstkSceneManager.h>
#include <imstkSimulationManager.h>
#include <imstkObjectInteractionFactory.h>

#include "DynamicObject.h"

namespace simManager
{
std::shared_ptr<imstk::SimulationManager> sdk = nullptr;
std::function<void()> PostUpdate = nullptr;

void genSimManager(const int numThreads)
{
    // Create SDK
    std::shared_ptr<imstk::SimManagerConfig> simConfig =
        std::make_shared<imstk::SimManagerConfig>();
    simConfig->simulationMode = imstk::SimulationMode::RunInBackgroundAsync;
    simConfig->threadPoolSize = numThreads;
    simConfig->enableStdoutLogging = false;
    simConfig->enableFileLogging = false;
    sdk = std::make_shared<imstk::SimulationManager>(simConfig);
    // Create the default scene
    std::shared_ptr<imstk::Scene> scene = sdk->createNewScene("Scene");
    sdk->setActiveScene(scene);
    sdk->getSceneManager(scene)->setPostUpdateCallback([&](imstk::Module* module) {
        if (PostUpdate != nullptr)
            PostUpdate();
    });
}

void initSimManager()
{
    if (sdk != nullptr)
        sdk->initialize();
    else
        LOG(WARNING) << "Cannot initialize SimulationManager, does not exist";
}

void setSimManagerPostUpdateCallback(void (*func)())
{
    PostUpdate = func;
}

void addInteractionPair(int objAInstanceId,
                        int objBInstanceId,
                        int interactionType,
                        int collisionDetectionType)
{
    if (!dynamicObjects.count(objAInstanceId))
    {
        LOG(INFO) << "Could not find object of instance id: " << objAInstanceId
                  << " for collision pair";
        return;
    }
    std::shared_ptr<imstk::CollidingObject> objA =
        std::dynamic_pointer_cast<imstk::CollidingObject>(dynamicObjects[objAInstanceId]);
    if (!dynamicObjects.count(objBInstanceId))
    {
        LOG(INFO) << "Could not find object of instance id: " << objBInstanceId
                  << " for collision pair";
        return;
    }
    std::shared_ptr<imstk::CollidingObject> objB =
        std::dynamic_pointer_cast<imstk::CollidingObject>(dynamicObjects[objBInstanceId]);

    /* LOG(INFO) << "Collision pair added to scene: (\n\t"
         << "Interaction Type: " << interactionType << "\n\t"
         << "Collision Detection Type: " << collisionDetectionType << "\n\t"
         << "ObjA ID: " << objAInstanceId << "\n\t"
         << "ObjB ID: " << objBInstanceId;*/

    std::shared_ptr<imstk::ObjectInteractionPair> interactionPair =
        imstk::makeObjectInteractionPair(
            objA,
            objB,
            static_cast<imstk::InteractionType>(interactionType),
            static_cast<imstk::CollisionDetection::Type>(collisionDetectionType));

    if (interactionPair == nullptr)
    {
        LOG(INFO) << "Failed to create interaction pair: (\n\t"
                  << "Interaction Type: " << interactionType << "\n\t"
                  << "Collision Detection Type: " << collisionDetectionType << "\n\t"
                  << "ObjA ID: " << objAInstanceId << "\n\t"
                  << "ObjB ID: " << objBInstanceId;
        return;
    }

    sdk->getActiveScene()->getCollisionGraph()->addInteraction(interactionPair);
}

void startSimManager()
{
    if (sdk != nullptr)
        sdk->start(imstk::SimulationStatus::Running, imstk::Renderer::Mode::Empty);
    else
        LOG(FATAL) << "Cannot start SimulationManager, does not exist";
}

void advanceFrame(const double dt)
{
    sdk->getActiveScene()->advance(dt);
}

double getActiveSceneDt()
{
    return sdk->getActiveScene()->getElapsedTime();
}

void pauseSimManager()
{
    sdk->pause();
}

void resumeSimManager()
{
    sdk->run();
}

imstk::ModuleStatus getSimManagerStatus()
{
    return sdk->getStatus();
}

void stopSimManager()
{
    if (sdk != nullptr)
        sdk->end();
    else
        LOG(FATAL) << "Cannot stop SimulationManager, does not exist";
}

void deleteSimManager()
{
    sdk = nullptr;
}
} // namespace simManager