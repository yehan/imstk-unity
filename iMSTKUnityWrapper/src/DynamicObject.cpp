#pragma once
#include "DynamicObject.h"

#include <imstkDynamicObject.h>
#include <imstkDynamicalModel.h>
#include <imstkLogger.h>
#include <imstkOneToOneMap.h>
#include <imstkTetraTriangleMap.h>

#include "Geometry.h"

int dynamicObjIter = 0;
std::unordered_map<int, std::shared_ptr<imstk::DynamicObject>> dynamicObjects;

// Returns a valid map for the geometries (if none found returns nullptr)
static std::shared_ptr<imstk::GeometryMap> findValidMap(std::shared_ptr<imstk::Geometry> geom1,
                                                        std::shared_ptr<imstk::Geometry> geom2)
{
    std::shared_ptr<imstk::OneToOneMap> oneToOneMap =
        std::make_shared<imstk::OneToOneMap>(geom1, geom2);
    if (oneToOneMap->isValid())
        return oneToOneMap;
    std::shared_ptr<imstk::TetraTriangleMap> tetraTriangleMap =
        std::make_shared<imstk::TetraTriangleMap>(geom1, geom2);
    tetraTriangleMap->compute();
    if (tetraTriangleMap->isValid())
        return tetraTriangleMap;
    LOG(WARNING) << "Failed to map geometry";
    return nullptr;
}

int genDynamicObject()
{
    const int key = dynamicObjIter;
    dynamicObjects[key] = nullptr;
    dynamicObjIter++;
    return key;
}

bool genDynamicObjectWithHandle(const int objectHandle)
{
    if (dynamicObjects.count(objectHandle))
        return false;
    dynamicObjects[objectHandle] = nullptr;
    return true;
}

void setDynamicObjectVisualGeometry(const int objectHandle, const int geometryHandle)
{
    dynamicObjects[objectHandle]->setVisualGeometry(geometries[geometryHandle]);
}

void setDynamicObjectPhysicsGeometry(const int objectHandle, const int geometryHandle)
{
    dynamicObjects[objectHandle]->setPhysicsGeometry(geometries[geometryHandle]);
    dynamicObjects[objectHandle]->getDynamicalModel()->setModelGeometry(geometries[geometryHandle]);
}

void setDynamicObjectCollisionGeometry(const int objectHandle, const int geometryHandle)
{
    dynamicObjects[objectHandle]->setCollidingGeometry(geometries[geometryHandle]);
}

void addDynamicObjectCollidingToVisualMap(const int objectHandle)
{
    std::shared_ptr<imstk::DynamicObject> object = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::GeometryMap> geometryMap =
        findValidMap(object->getCollidingGeometry(), object->getVisualGeometry());
    if (geometryMap != nullptr)
        object->setCollidingToVisualMap(geometryMap);
}

void addDynamicObjectPhysicsToCollidingMap(const int objectHandle)
{
    std::shared_ptr<imstk::DynamicObject> object = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::GeometryMap> geometryMap =
        findValidMap(object->getPhysicsGeometry(), object->getCollidingGeometry());
    if (geometryMap != nullptr)
        object->setPhysicsToCollidingMap(geometryMap);
}

void addDynamicObjectPhysicsToVisualMap(const int objectHandle)
{
    std::shared_ptr<imstk::DynamicObject> object = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::GeometryMap> geometryMap =
        findValidMap(object->getPhysicsGeometry(), object->getVisualGeometry());
    if (geometryMap != nullptr)
        object->setPhysicsToVisualMap(geometryMap);
}

void updateDynamicObjectVisualGeometry(const int objectHandle, float* vertexBufferPtr)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::PointSet> pointSet =
        std::dynamic_pointer_cast<imstk::PointSet>(dynamicObject->getVisualGeometry());
    // Copy vertices (will eliminate in the future)
    const imstk::StdVectorOfVec3d& positions = pointSet->getVertexPositions();
    for (size_t i = 0; i < positions.size(); i++)
    {
        vertexBufferPtr[i * 3] = static_cast<float>(positions[i][0]);
        vertexBufferPtr[i * 3 + 1] = static_cast<float>(positions[i][1]);
        vertexBufferPtr[i * 3 + 2] = static_cast<float>(positions[i][2]);
    }
}

void deleteDynamicObject(const int objectHandle)
{
    if (dynamicObjects.count(objectHandle))
    {
        dynamicObjects[objectHandle] = nullptr;
        dynamicObjects.erase(objectHandle);
    }
}