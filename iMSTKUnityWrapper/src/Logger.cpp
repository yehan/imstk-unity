#include "Logger.h"

#include <imstkLogger.h>

void (*PrintStringExt)(const char* str) = nullptr;

void genLogger()
{
    imstk::Logger& logger = imstk::Logger::getInstance();
    logger.initialize();
    logger.addFileSink("unity", "./");
    logger.addSink(std::make_unique<UnityG3Sink>(), &UnityG3Sink::ReceiveLogMessage);
}

void deleteLogger()
{
    imstk::Logger::getInstance().destroy();
}

void setSink(void (*func)(const char* str))
{
    PrintStringExt = func;
}