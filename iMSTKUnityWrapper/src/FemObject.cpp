#include "FemObject.h"

#include <imstkBackwardEuler.h>
#include <imstkCamera.h>
#include <imstkCollisionGraph.h>
#include <imstkConjugateGradient.h>
#include <imstkDeformableObject.h>
#include <imstkFEMDeformableBodyModel.h>
#include <imstkNewtonSolver.h>
#include <imstkNonLinearSystem.h>
#include <imstkScene.h>
#include <imstkSimulationManager.h>

#include "DynamicObject.h"
#include "SimManager.h"

void configureFemObject(const int objectHandle, const FemParams params)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    auto femModel =
        std::static_pointer_cast<imstk::FEMDeformableBodyModel>(dynamicObject->getDynamicalModel());
    const std::string configFilePath = params.configFilePath;
    if (configFilePath != "")
        femModel->configure(configFilePath);
    else
    {
        auto femConfig = std::make_shared<imstk::FEMModelConfig>();
        femConfig->m_femMethod = params.femMethodType;
        femConfig->m_hyperElasticMaterialType = params.hyperelasticMaterialType;
        femConfig->m_dampingMassCoefficient = params.dampingMassCoefficient;
        femConfig->m_dampingStiffnessCoefficient = params.dampingStiffnessCoefficient;
        femConfig->m_dampingLaplacianCoefficient = params.dampingLaplacianCoefficient;
        femConfig->m_deformationCompliance = params.deformationCompliance;
        femConfig->m_compressionResistance = params.compressionResistance;
        femConfig->m_inversionThreshold = params.inversionThreshold;
        femConfig->m_gravity = params.gravityAccel;
        femConfig->m_fixedNodeIds.clear();
        for (size_t i = 0; i < params.fixedIndexCount; i++)
        {
            femConfig->m_fixedNodeIds.push_back(params.fixedIndices[i]);
        }

        femModel->configure(femConfig);
        if (params.dt == 0.0)
            femModel->setTimeStepSizeType(imstk::TimeSteppingType::RealTime);
        else
        {
            femModel->getTimeIntegrator()->setDefaultTimestepSize(params.dt);
            femModel->getTimeIntegrator()->setTimestepSizeToDefault();
            femModel->setTimeStepSizeType(imstk::TimeSteppingType::Fixed);
        }
    }
}

void initFemObject(const int objectHandle)
{
    // Create Model
    auto femModel = std::make_shared<imstk::FEMDeformableBodyModel>();
    femModel->setTimeIntegrator(std::make_shared<imstk::BackwardEuler>(0.001));

    // Create Object
    dynamicObjects[objectHandle] =
        std::make_shared<imstk::FeDeformableObject>(std::to_string(objectHandle));
    dynamicObjects[objectHandle]->setDynamicalModel(femModel);

    // Add to scene
    auto scene = simManager::sdk->getActiveScene();
    scene->addSceneObject(dynamicObjects[objectHandle]);
}