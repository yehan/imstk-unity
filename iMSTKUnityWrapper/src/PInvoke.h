#pragma once

#ifdef WIN32
#    define NOMINMAX
#    include <Windows.h>
#    define IMSTK_EXPORT __declspec(dllexport)
#else
// WIP
#    define IMSTK_EXPORT __attribute__((visibility("default")))
#endif

extern "C"
{
    ///
    /// \brief Called when library is attached, currently doesn't  do anything
    /// sometimes useful for debug
    ///
#ifdef WIN32
    BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved);
#endif
}