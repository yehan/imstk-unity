#include "RigidObject.h"

#include <imstkCamera.h>
#include <imstkCollisionGraph.h>
#include <imstkRigidBodyModel.h>
#include <imstkRigidObject.h>
#include <imstkScene.h>
#include <imstkSimulationManager.h>

#include "DynamicObject.h"
#include "SimManager.h"

void configureRigidObject(int objectHandle, RigidParams params)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    auto rigidModel =
        std::static_pointer_cast<imstk::RigidBodyModel>(dynamicObject->getDynamicalModel());

    auto rigidConfig = std::make_shared<imstk::RigidBodyConfig>();
    rigidConfig->m_rigidBodyType = imstk::RigidBodyType::Kinematic;
    rigidModel->configure(rigidConfig);
}

void initRigidObject(int objectHandle)
{
    // Create Model
    auto rigidModel = std::make_shared<imstk::RigidBodyModel>();

    // Create Object
    dynamicObjects[objectHandle] =
        std::make_shared<imstk::RigidObject>(std::to_string(objectHandle));
    dynamicObjects[objectHandle]->setDynamicalModel(rigidModel);

    // Add to scene
    auto scene = simManager::sdk->getActiveScene();
    scene->addSceneObject(dynamicObjects[objectHandle]);
}