#include "MeshIO.h"

#include <imstkMeshIO.h>

#include "Geometry.h"

int readGeometry(const char* filePath)
{
    const int key = geometryIter;
    geometries[key] = imstk::MeshIO::read(std::string(filePath));
    geometryIter++;
    return key;
}