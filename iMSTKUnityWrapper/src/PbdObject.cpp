#include "PbdObject.h"

#include <imstkCamera.h>
#include <imstkCollisionGraph.h>
#include <imstkPbdModel.h>
#include <imstkPbdObject.h>
#include <imstkPbdSolver.h>
#include <imstkScene.h>
#include <imstkSimulationManager.h>

#include "DynamicObject.h"
#include "SimManager.h"

void configurePbdObject(const int objectHandle, const PbdParams params)
{
    // Create Parameters
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    auto pbdModel = std::static_pointer_cast<imstk::PbdModel>(dynamicObject->getDynamicalModel());

    auto pbdConfig = std::make_shared<imstk::PBDModelConfig>();
    if (params.distanceStiffness != -1.0)
        pbdConfig->enableConstraint(imstk::PbdConstraint::Type::Distance, params.distanceStiffness);
    if (params.dihedralStiffness != -1.0)
        pbdConfig->enableConstraint(imstk::PbdConstraint::Type::Dihedral, params.dihedralStiffness);
    if (params.areaStiffness != -1.0)
        pbdConfig->enableConstraint(imstk::PbdConstraint::Type::Area, params.areaStiffness);
    if (params.volumeStiffness != -1.0)
        pbdConfig->enableConstraint(imstk::PbdConstraint::Type::Volume, params.volumeStiffness);
    if (params.youngsModulus != -1.0)
    {
        pbdConfig->enableFEMConstraint(imstk::PbdConstraint::Type::FEMTet, params.materialType);
        pbdConfig->femParams->m_YoungModulus = params.youngsModulus;
        pbdConfig->femParams->m_PoissonRatio = params.possionRatio;
        pbdConfig->femParams->m_mu = params.mu;
        pbdConfig->femParams->m_lambda = params.lambda;
    }
    pbdConfig->m_fixedNodeIds.clear();
    for (size_t i = 0; i < params.fixedIndexCount; i++)
    {
        pbdConfig->m_fixedNodeIds.push_back(params.fixedIndices[i]);
    }
    pbdConfig->m_uniformMassValue = params.uniformMassValue;
    pbdConfig->m_gravity =
        imstk::Vec3d(params.gravityAccel.x, params.gravityAccel.y, params.gravityAccel.z);
    pbdConfig->m_iterations = params.numIterations;
    pbdConfig->m_collisionIterations = params.numCollisionIterations;
    pbdConfig->collisionParams->m_stiffness = params.contactStiffness;
    pbdConfig->collisionParams->m_proximity = params.proximity;
    pbdConfig->m_defaultDt = params.dt;

    pbdModel->configure(pbdConfig);

    if (params.dt == 0.0)
        pbdModel->setTimeStepSizeType(imstk::TimeSteppingType::RealTime);
    else
        pbdModel->setTimeStepSizeType(imstk::TimeSteppingType::Fixed);
}

void initPbdObject(const int objectHandle)
{
    // Create Model
    auto pbdModel = std::make_shared<imstk::PbdModel>();

    // Create Object
    dynamicObjects[objectHandle] = std::make_shared<imstk::PbdObject>(std::to_string(objectHandle));
    dynamicObjects[objectHandle]->setDynamicalModel(pbdModel);

    // Add to scene
    auto scene = simManager::sdk->getActiveScene();
    scene->addSceneObject(dynamicObjects[objectHandle]);
}

void setPbdStateVertex(const int objectHandle, const int index, Vec3f vertex)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::PbdModel> model =
        std::static_pointer_cast<imstk::PbdModel>(dynamicObject->getDynamicalModel());
    model->getCurrentState()->setVertexPosition(index, imstk::Vec3d(vertex.x, vertex.y, vertex.z));
}

void translateVertices(const int objectHandle, Vec3f dx)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::PbdModel> model =
        std::static_pointer_cast<imstk::PbdModel>(dynamicObject->getDynamicalModel());
    imstk::StdVectorOfVec3d& positions = *model->getCurrentState()->getPositions();
    const imstk::Vec3d translation = imstk::Vec3d(dx.x, dx.y, dx.z);
    for (size_t i = 0; i < positions.size(); i++)
    {
        positions[i] += translation;
    }
}

Vec3f getPbdStateVertex(const int objectHandle, const int index)
{
    std::shared_ptr<imstk::DynamicObject> dynamicObject = dynamicObjects[objectHandle];
    std::shared_ptr<imstk::PbdModel> model =
        std::static_pointer_cast<imstk::PbdModel>(dynamicObject->getDynamicalModel());
    const imstk::Vec3d& pt = model->getCurrentState()->getVertexPosition(index);

    return Vec3f{ static_cast<float>(pt.x()),
                  static_cast<float>(pt.y()),
                  static_cast<float>(pt.z()) };
}