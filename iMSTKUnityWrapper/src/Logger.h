#pragma once
#include <string>

#include "PInvoke.h"
#include "g3log/g3log.hpp"

extern void (*PrintStringExt)(const char* str);

class UnityG3Sink
{
public:
    std::string GetColor(const LEVELS level) const
    {
        if (level.value == WARNING.value)
            return "orange";
        if (level.value == DEBUG.value)
            return "green";
        if (g3::internal::wasFatal(level))
            return "red";
        return "white";
    }

    void ReceiveLogMessage(g3::LogMessageMover logEntry)
    {
        const std::string messageStr = "<color=" + GetColor(logEntry.get()._level) + '>' +
                                       logEntry.get().toString() + "</color>";
        if (PrintStringExt != nullptr)
            PrintStringExt(messageStr.c_str());
    }
};

extern "C"
{
    ///
    /// \brief Creates the logger
    ///
    IMSTK_EXPORT void genLogger();

    ///
    /// \brief Cleans up the logger
    ///
    IMSTK_EXPORT void deleteLogger();

    ///
    /// \brief Registers a function to call to print
    ///
    IMSTK_EXPORT void setSink(void (*func)(const char* str));
}