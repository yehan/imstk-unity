#pragma once
#include <memory>
#include <unordered_map>

#include "PInvoke.h"

namespace imstk
{
class DynamicObject;
class PointSet;
} // namespace imstk

extern int dynamicObjIter;
extern std::unordered_map<int, std::shared_ptr<imstk::DynamicObject>> dynamicObjects;

extern "C"
{
    ///
    /// \brief Allocates the dynamic object
    ///
    IMSTK_EXPORT int genDynamicObject();

    ///
    /// \brief Allocates the dynamic object with specified handle
    ///
    IMSTK_EXPORT bool genDynamicObjectWithHandle(const int objectHandle);

    ///
    /// \brief Sets the visual geometry
    ///
    IMSTK_EXPORT void setDynamicObjectVisualGeometry(const int objectHandle,
                                                     const int geometryHandle);

    ///
    /// \brief Sets the physics geometry
    ///
    IMSTK_EXPORT void setDynamicObjectPhysicsGeometry(const int objectHandle,
                                                      const int geometryHandle);

    ///
    /// \brief Sets the collision geometry
    ///
    IMSTK_EXPORT void setDynamicObjectCollisionGeometry(const int objectHandle,
                                                        const int geometryHandle);

    ///
    /// \brief Adds a mapping between the current collision and visual geometries
    ///
    IMSTK_EXPORT void addDynamicObjectCollidingToVisualMap(const int objectHandle);

    ///
    /// \brief Adds a mapping between the current physics and collision geometries
    ///
    IMSTK_EXPORT void addDynamicObjectPhysicsToCollidingMap(const int objectHandle);

    ///
    /// \brief Adds a mapping between the current physics and visual geometries
    ///
    IMSTK_EXPORT void addDynamicObjectPhysicsToVisualMap(const int objectHandle);

    ///
    /// \brief Exports the vertices of a visual geometry
    /// (this is the only unity specific function in our c api, it will likely change)
    ///
    IMSTK_EXPORT void updateDynamicObjectVisualGeometry(const int objectHandle,
                                                        float* vertexBufferPtr);

    ///
    /// \brief Frees the dynamic object
    ///
    IMSTK_EXPORT void deleteDynamicObject(const int objectHandle);
}