#include "Geometry.h"

#include <imstkGeometryUtilities.h>
#include <imstkHexahedralMesh.h>
#include <imstkLineMesh.h>
#include <imstkSurfaceMesh.h>
#include <imstkTetrahedralMesh.h>

int geometryIter = 0;
std::unordered_map<int, std::shared_ptr<imstk::Geometry>> geometries;

static std::shared_ptr<imstk::Geometry> NewGeometry(GeometryType type)
{
    switch (type)
    {
        case GeometryType::PointSet:
            return std::make_shared<imstk::PointSet>();
        case GeometryType::LineMesh:
            return std::make_shared<imstk::LineMesh>();
        case GeometryType::SurfaceMesh:
            return std::make_shared<imstk::SurfaceMesh>();
        case GeometryType::TetrahedralMesh:
            return std::make_shared<imstk::TetrahedralMesh>();
        case GeometryType::HexahedralMesh:
            return std::make_shared<imstk::HexahedralMesh>();
        default:
            return nullptr;
    }
}

int genGeometry(const GeometryType type)
{
    const int key = geometryIter;
    geometries[key] = NewGeometry(type);
    geometryIter++;
    return key;
}

bool genGeometryWithHandle(const GeometryType type, const int objHandle)
{
    if (geometries.count(objHandle))
        return false;
    geometries[objHandle] = NewGeometry(type);
    return true;
}

int genGeometryFromPrimitiveGeometry(PrimitiveGeometry primGeometry)
{
    const int key = geometryIter;
    geometries[key] = primitiveToImstkGeometry(primGeometry);
    geometryIter++;
    return key;
}

GeometryInfo getGeometryInfo(const int objHandle)
{
    return imstkGeometryInfo(geometries[objHandle]);
}

void exportGeometry(const int objHandle, PrimitiveGeometry primGeometry)
{
    // Fill out the buffers
    imstkToPrimitiveGeometry(geometries[objHandle], primGeometry);
}

int extractSurfaceMesh(const int volHandle, const bool flipNormals)
{
    std::shared_ptr<imstk::Geometry> volMesh = geometries[volHandle];

    std::shared_ptr<imstk::SurfaceMesh> outputMesh = std::make_shared<imstk::SurfaceMesh>();
    if (volMesh->getType() == imstk::Geometry::Type::TetrahedralMesh)
        std::dynamic_pointer_cast<imstk::TetrahedralMesh>(volMesh)->extractSurfaceMesh(outputMesh,
                                                                                       true);
    else if (volMesh->getType() == imstk::Geometry::Type::HexahedralMesh)
        std::dynamic_pointer_cast<imstk::HexahedralMesh>(volMesh)->extractSurfaceMesh(outputMesh);
    if (flipNormals)
        outputMesh->flipNormals();
    const int key = geometryIter;
    geometries[key] = outputMesh;
    geometryIter++;
    return key;
}

void reverseSurfaceMesh(const int surfHandle)
{
    std::shared_ptr<imstk::SurfaceMesh> surfMesh =
        std::static_pointer_cast<imstk::SurfaceMesh>(geometries[surfHandle]);
    if (surfMesh != nullptr)
        surfMesh->flipNormals();
}

void testEnclosedPoints(const int surfHandle, const int pointSetHandle, bool* isInside)
{
    std::shared_ptr<imstk::SurfaceMesh> surfMesh =
        std::dynamic_pointer_cast<imstk::SurfaceMesh>(geometries[surfHandle]);
    std::shared_ptr<imstk::PointSet> pointSetMesh =
        std::dynamic_pointer_cast<imstk::PointSet>(geometries[pointSetHandle]);

    if (surfMesh != nullptr && pointSetMesh != nullptr)
    {
        std::vector<bool> isInsideVec;
        isInsideVec.resize(pointSetMesh->getNumVertices());
        imstk::GeometryUtils::testEnclosedPoints(isInsideVec, surfMesh, pointSetMesh);
        /*for (UINT i = 0; i < isInsideVec.size(); i++)
        {
            isInside[i] = isInsideVec[i];
        }*/
        std::copy(isInsideVec.begin(), isInsideVec.end(), isInside);
    }
}

void deleteGeometry(const int objHandle)
{
    if (geometries.count(objHandle))
    {
        geometries[objHandle] = nullptr;
        geometries.erase(objHandle);
    }
    else
        LOG(INFO) << "Trying to delete geometry " << objHandle << " but it does not exist.";
}

static void copyVerticesToBuffer(const imstk::StdVectorOfVec3d& pos, float* vertexBufferPtr)
{
    for (size_t i = 0; i < pos.size(); i++)
    {
        vertexBufferPtr[i * 3] = pos[i][0];
        vertexBufferPtr[i * 3 + 1] = pos[i][1];
        vertexBufferPtr[i * 3 + 2] = pos[i][2];
    }
}
static void copyBufferToVertices(float* vertexBufferPtr,
                                 const int vertexCount,
                                 imstk::StdVectorOfVec3d& pos)
{
    pos.resize(vertexCount);
    for (int i = 0; i < vertexCount; i++)
    {
        pos[i][0] = vertexBufferPtr[i * 3];
        pos[i][1] = vertexBufferPtr[i * 3 + 1];
        pos[i][2] = vertexBufferPtr[i * 3 + 2];
    }
}
template <size_t N>
static void copyIndicesToBuffer(const std::vector<std::array<size_t, N>>& cells,
                                int* indexBufferPtr)
{
    for (size_t i = 0; i < cells.size(); i++)
    {
        for (size_t j = 0; j < N; j++)
        {
            indexBufferPtr[i * N + j] = static_cast<int>(cells[i][j]);
        }
    }
}
template <size_t N>
static void copyBufferToIndices(int* indexBufferPtr,
                                const int cellCount,
                                std::vector<std::array<size_t, N>>& cells)
{
    cells.resize(cellCount);
    for (size_t i = 0; i < cellCount; i++)
    {
        for (size_t j = 0; j < N; j++)
        {
            cells[i][j] = indexBufferPtr[i * N + j];
        }
    }
}

std::shared_ptr<imstk::Geometry> primitiveToImstkGeometry(PrimitiveGeometry primGeometry)
{
    std::shared_ptr<imstk::Geometry> results = nullptr;
    if (primGeometry.info.type == GeometryType::PointSet)
    {
        imstk::StdVectorOfVec3d positions;
        copyBufferToVertices(
            primGeometry.vertexBufferPtr, primGeometry.info.vertexCount, positions);

        std::shared_ptr<imstk::PointSet> pointSetResults = std::make_shared<imstk::PointSet>();
        pointSetResults->setInitialVertexPositions(positions);
        pointSetResults->setVertexPositions(positions);
        results = pointSetResults;
    }
    else if (primGeometry.info.type == GeometryType::LineMesh)
    {
        imstk::StdVectorOfVec3d positions;
        copyBufferToVertices(
            primGeometry.vertexBufferPtr, primGeometry.info.vertexCount, positions);

        std::vector<imstk::LineMesh::LineArray> cells;
        copyBufferToIndices(primGeometry.indexBufferPtr, primGeometry.info.cellCount, cells);

        std::shared_ptr<imstk::LineMesh> lineResults = std::make_shared<imstk::LineMesh>();
        lineResults->setInitialVertexPositions(positions);
        lineResults->setVertexPositions(positions);
        lineResults->setLinesVertices(cells);
        results = lineResults;
    }
    else if (primGeometry.info.type == GeometryType::SurfaceMesh)
    {
        imstk::StdVectorOfVec3d positions;
        copyBufferToVertices(
            primGeometry.vertexBufferPtr, primGeometry.info.vertexCount, positions);

        std::vector<imstk::SurfaceMesh::TriangleArray> cells(primGeometry.info.cellCount);
        copyBufferToIndices(primGeometry.indexBufferPtr, primGeometry.info.cellCount, cells);

        std::shared_ptr<imstk::SurfaceMesh> surfaceResults = std::make_shared<imstk::SurfaceMesh>();
        surfaceResults->setInitialVertexPositions(positions);
        surfaceResults->setVertexPositions(positions);
        surfaceResults->setTrianglesVertices(cells);
        results = surfaceResults;
    }
    else if (primGeometry.info.type == GeometryType::TetrahedralMesh)
    {
        imstk::StdVectorOfVec3d positions;
        copyBufferToVertices(
            primGeometry.vertexBufferPtr, primGeometry.info.vertexCount, positions);

        std::vector<imstk::TetrahedralMesh::TetraArray> cells(primGeometry.info.cellCount);
        copyBufferToIndices(primGeometry.indexBufferPtr, primGeometry.info.cellCount, cells);

        std::shared_ptr<imstk::TetrahedralMesh> tetResults =
            std::make_shared<imstk::TetrahedralMesh>();
        tetResults->setInitialVertexPositions(positions);
        tetResults->setVertexPositions(positions);
        tetResults->setTetrahedraVertices(cells);
        results = tetResults;
    }
    else if (primGeometry.info.type == GeometryType::HexahedralMesh)
    {
        imstk::StdVectorOfVec3d positions;
        copyBufferToVertices(
            primGeometry.vertexBufferPtr, primGeometry.info.vertexCount, positions);

        std::vector<imstk::HexahedralMesh::HexaArray> cells(primGeometry.info.cellCount);
        copyBufferToIndices(primGeometry.indexBufferPtr, primGeometry.info.cellCount, cells);

        std::shared_ptr<imstk::HexahedralMesh> hexResults =
            std::make_shared<imstk::HexahedralMesh>();
        hexResults->setInitialVertexPositions(positions);
        hexResults->setVertexPositions(positions);
        hexResults->setHexahedraVertices(cells);
        results = hexResults;
    }
    else
        LOG(WARNING) << "Trying to convert PrimitiveGeometry to unknown type";
    return results;
}

void imstkToPrimitiveGeometry(std::shared_ptr<imstk::Geometry> imstkGeometry,
                              PrimitiveGeometry& primGeometry)
{
    primGeometry.info = imstkGeometryInfo(imstkGeometry);
    imstk::Geometry::Type geomType = imstkGeometry->getType();
    if (geomType == imstk::Geometry::Type::PointSet)
    {
        std::shared_ptr<imstk::PointSet> pointSet =
            std::dynamic_pointer_cast<imstk::PointSet>(imstkGeometry);
        copyVerticesToBuffer(pointSet->getVertexPositions(), primGeometry.vertexBufferPtr);
    }
    else if (geomType == imstk::Geometry::Type::LineMesh)
    {
        std::shared_ptr<imstk::LineMesh> lineMesh =
            std::dynamic_pointer_cast<imstk::LineMesh>(imstkGeometry);
        copyVerticesToBuffer(lineMesh->getVertexPositions(), primGeometry.vertexBufferPtr);
        copyIndicesToBuffer(lineMesh->getLinesVertices(), primGeometry.indexBufferPtr);
    }
    else if (geomType == imstk::Geometry::Type::SurfaceMesh)
    {
        std::shared_ptr<imstk::SurfaceMesh> surfMesh =
            std::dynamic_pointer_cast<imstk::SurfaceMesh>(imstkGeometry);
        copyVerticesToBuffer(surfMesh->getVertexPositions(), primGeometry.vertexBufferPtr);
        copyIndicesToBuffer(surfMesh->getTrianglesVertices(), primGeometry.indexBufferPtr);
    }
    else if (geomType == imstk::Geometry::Type::TetrahedralMesh)
    {
        std::shared_ptr<imstk::TetrahedralMesh> tetMesh =
            std::dynamic_pointer_cast<imstk::TetrahedralMesh>(imstkGeometry);
        copyVerticesToBuffer(tetMesh->getVertexPositions(), primGeometry.vertexBufferPtr);
        copyIndicesToBuffer(tetMesh->getTetrahedraVertices(), primGeometry.indexBufferPtr);
    }
    else if (geomType == imstk::Geometry::Type::HexahedralMesh)
    {
        std::shared_ptr<imstk::HexahedralMesh> hexMesh =
            std::dynamic_pointer_cast<imstk::HexahedralMesh>(imstkGeometry);
        copyVerticesToBuffer(hexMesh->getVertexPositions(), primGeometry.vertexBufferPtr);
        copyIndicesToBuffer(hexMesh->getHexahedraVertices(), primGeometry.indexBufferPtr);
    }
}

GeometryInfo imstkGeometryInfo(std::shared_ptr<imstk::Geometry> geometry)
{
    GeometryInfo info;
    imstk::Geometry::Type geomType = geometry->getType();
    if (geomType == imstk::Geometry::Type::PointSet)
    {
        std::shared_ptr<imstk::PointSet> pointSet =
            std::static_pointer_cast<imstk::PointSet>(geometry);
        info.vertexCount = static_cast<int>(pointSet->getNumVertices());
        info.cellCount = 0;
        info.type = GeometryType::PointSet;
    }
    else if (geomType == imstk::Geometry::Type::LineMesh)
    {
        std::shared_ptr<imstk::LineMesh> lineMesh =
            std::static_pointer_cast<imstk::LineMesh>(geometry);
        info.vertexCount = static_cast<int>(lineMesh->getNumVertices());
        info.cellCount = static_cast<int>(lineMesh->getNumLines());
        info.type = GeometryType::LineMesh;
    }
    else if (geomType == imstk::Geometry::Type::SurfaceMesh)
    {
        std::shared_ptr<imstk::SurfaceMesh> surfMesh =
            std::static_pointer_cast<imstk::SurfaceMesh>(geometry);
        info.vertexCount = static_cast<int>(surfMesh->getNumVertices());
        info.cellCount = static_cast<int>(surfMesh->getNumTriangles());
        info.type = GeometryType::SurfaceMesh;
    }
    else if (geomType == imstk::Geometry::Type::TetrahedralMesh)
    {
        std::shared_ptr<imstk::TetrahedralMesh> tetMesh =
            std::static_pointer_cast<imstk::TetrahedralMesh>(geometry);
        info.vertexCount = static_cast<int>(tetMesh->getNumVertices());
        info.cellCount = static_cast<int>(tetMesh->getNumTetrahedra());
        info.type = GeometryType::TetrahedralMesh;
    }
    else if (geomType == imstk::Geometry::Type::HexahedralMesh)
    {
        std::shared_ptr<imstk::HexahedralMesh> hexMesh =
            std::static_pointer_cast<imstk::HexahedralMesh>(geometry);
        info.vertexCount = static_cast<int>(hexMesh->getNumVertices());
        info.cellCount = static_cast<int>(hexMesh->getNumHexahedra());
        info.type = GeometryType::HexahedralMesh;
    }
    else
        LOG(WARNING) << "Trying to get info on unknown geometry type";
    return info;
}