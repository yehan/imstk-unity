#pragma once
#include "PInvoke.h"

extern "C"
{
    ///
    /// \brief Generates and reads the geomery from the given file, returns handle to mesh
    ///
    IMSTK_EXPORT int readGeometry(const char* filePath);
}