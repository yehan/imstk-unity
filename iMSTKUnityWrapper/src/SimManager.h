#pragma once
#include <imstkModule.h>

#include <functional>
#include <memory>

#include "PInvoke.h"

namespace imstk
{
class Module;
class SimulationManager;
} // namespace imstk

namespace simManager
{
extern std::shared_ptr<imstk::SimulationManager> sdk;
extern std::function<void()> PostUpdate;

extern "C"
{
    ///
    /// \brief Creates the SimulationManager
    ///
    IMSTK_EXPORT void genSimManager(const int numThreads);

    ///
    /// \brief Initializes the SimulationManager
    ///
    IMSTK_EXPORT void initSimManager();

    ///
    /// \brief Sets the function to call after updating the SimulationManager
    ///
    IMSTK_EXPORT void setSimManagerPostUpdateCallback(void (*func)());

    ///
    /// \brief Adds an interaction pair to the collision graph
    ///
    IMSTK_EXPORT void addInteractionPair(int objAInstanceId,
                                         int objBInstanceId,
                                         int interactionType,
                                         int collisionDetectionType);

    ///
    /// \brief Starts the SimulationManager
    ///
    IMSTK_EXPORT void startSimManager();

    ///
    /// \brief Advances a single frame on the active scene
    ///
    IMSTK_EXPORT void advanceFrame(const double dt);

    ///
    /// \brief Gets the elapsed time of the active scene
    ///
    IMSTK_EXPORT double getActiveSceneDt();

    ///
    /// \brief Pauses the SimulationManager
    ///
    IMSTK_EXPORT void pauseSimManager();

    ///
    /// \brief Resumes the SimulationManager from pause
    ///
    IMSTK_EXPORT void resumeSimManager();

    ///
    /// \brief Returns the status of the SimulationManager
    ///
    IMSTK_EXPORT imstk::ModuleStatus getSimManagerStatus();

    ///
    /// \brief Stops the SimulationManager
    ///
    IMSTK_EXPORT void stopSimManager();

    ///
    /// \brief Deletes the SimulationManager
    ///
    IMSTK_EXPORT void deleteSimManager();
}
}; // namespace simManager