Welcome to iMSTK-Unity documentation!
===============================

.. toctree:: 
   :maxdepth: 2
   :caption: User Guide

   documentation

.. toctree:: 
   :maxdepth: 0
   :caption: Release Notes
   
   releases   
   
.. toctree:: 
   :maxdepth: 1
   :caption: Licensing

   license